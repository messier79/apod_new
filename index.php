<?php
require_once "vendor/autoload.php";

use Helpers\Routing;
use Helpers\Session;
use Helpers\Tools;

try {
	Tools::redirectOldUrls();
	
    // For CSS, JS...
	Tools::handleFileTypes();

    Routing::handle();
} catch (\Exception $e) {

}
