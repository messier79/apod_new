<?php

namespace Config;

use Helpers\Session;

/**
 * Class Config
 * @package Config
 */
class Config
{
	/**
	 * @var array
	 */
	private $parameters;

	/**
	 * Config constructor.
	 * @param array $localParameters
	 */
	public function __construct()
	{
		$localParameters = [];

		include __DIR__ . '/../env.php';

		$this->parameters = [
			'env'                    => $localParameters['ENV'],
			'engine'                 => $localParameters['DB_ENGINE'],
			'host'                   => $localParameters['DB_HOST'],
			'name'                   => $localParameters['DB_NAME'],
			'user'                   => $localParameters['DB_USER'],
			'pass'                   => $localParameters['DB_PASS'],
			'charset'                => $localParameters['DB_CHARSET'],
			'twig_debug'             => $localParameters['TWIG_DEBUG'],
			'photos_directory'       => $localParameters['PHOTOS_DIRECTORY'],
			'smtp_host'              => $localParameters['SMTP_HOST'],
			'smtp_secure'            => $localParameters['SMTP_SECURE'],
			'smtp_port'              => $localParameters['SMTP_PORT'],
			'smtp_user'              => $localParameters['SMTP_USER'],
			'smtp_pass'              => $localParameters['SMTP_PASS'],
			'base_url'               => $localParameters['BASE_URL'],
			'backup_path'            => $localParameters['BACKUP_PATH'],
			'consumer_key'           => $localParameters['CONSUMER_KEY'],
			'consumer_secret'        => $localParameters['CONSUMER_SECRET'],
			'access_token'           => $localParameters['ACCESS_TOKEN'],
			'access_token_secret'    => $localParameters['ACCESS_TOKEN_SECRET'],
			'mastodon_url'           => $localParameters['MASTODON_URL'],
			'mastodon_token'         => $localParameters['MASTODON_TOKEN'],
			'tumblr_blog_name'       => $localParameters['TUMBLR_BLOG_NAME'],
			'tumblr_consumer_key'    => $localParameters['TUMBLR_CONSUMER_KEY'],
			'tumblr_consumer_secret' => $localParameters['TUMBLR_CONSUMER_SECRET'],
			'tumblr_token'           => $localParameters['TUMBLR_TOKEN'],
			'tumblr_token_secret'    => $localParameters['TUMBLR_TOKEN_SECRET'],
			'bluesky_username'       => $localParameters['BLUESKY_USERNAME'],
			'bluesky_password'       => $localParameters['BLUESKY_PASSWORD'],
			'cdn_url'                => $localParameters['CDN_URL'],
			'h_captcha'              => $localParameters['H_CAPTCHA'],
			'default_lang'           => 'en',
			'contact_recipient'      => 'apod@vidry.ca',
		];
		Session::set('db_settings', $this->parameters);
		Session::set('env', $localParameters['ENV']);
	}

	/**
	 * @return array
	 */
	public function getParameters()
	{
		return $this->parameters;
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function getParameter(string $key)
	{
		return $this->parameters[$key];
	}
}