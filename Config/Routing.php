<?php

namespace Config;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routing
{
	/**
	 * @var RouteCollection
	 */
	private $routes;

	public function __construct()
	{
		$this->routes = new RouteCollection();

		$this->buildRoutes();
		$this->buildAdminRoutes();
	}

	/**
	 * @return RouteCollection
	 */
	public function getRoutes(): RouteCollection
	{
		return $this->routes;
	}

	private function buildRoutes()
	{
		$this->routes->add(
			'home',
			new Route('/', ['_controller' => 'Controllers\\IndexController::index'])
		);

		$this->routes->add(
			'login',
			new Route('/login/{error}', ['_controller' => 'Controllers\\IndexController::login'])
		);

		$this->routes->add(
			'permissionDenied',
			new Route('/permission-denied', ['_controller' => 'Controllers\\IndexController::permissionDenied'])
		);

		$this->routes->add(
			'processLogin',
			new Route('/process-login', ['_controller' => 'Controllers\\IndexController::processLogin'])
		);

		$this->routes->add(
			'logout',
			new Route('/logout', ['_controller' => 'Controllers\\IndexController::logout'])
		);

		$this->routes->add(
			'index',
			new Route('/', ['_controller' => 'Controllers\\IndexController::index'])
		);

		$this->routes->add(
			'day',
			new Route('/day/{date}/{lang}', ['_controller' => 'Controllers\\IndexController::index'])
		);

		$this->routes->add(
			'about',
			new Route('/about', ['_controller' => 'Controllers\\IndexController::about'])
		);

		$this->routes->add(
			'calendar',
			new Route('/calendar/{year}/{month}', ['_controller' => 'Controllers\\IndexController::calendar'])
		);

		$this->routes->add(
			'mosaic',
			new Route('/mosaic/{year}', ['_controller' => 'Controllers\\IndexController::mosaic'])
		);

		$this->routes->add(
			'gallery',
			new Route('/gallery/{year}/{month}', ['_controller' => 'Controllers\\IndexController::calendar'])
		);

		$this->routes->add(
			'searchForm',
			new Route('/search-form', ['_controller' => 'Controllers\\IndexController::searchForm'])
		);

		$this->routes->add(
			'search',
			new Route('/search/{page}/{sort_by}/{sort_order}', ['_controller' => 'Controllers\\IndexController::search'])
		);

		$this->routes->add(
			'searchByTag',
			new Route('/search-by-tag/{tag}', ['_controller' => 'Controllers\\IndexController::searchByTag'])
		);

		$this->routes->add(
			'searchByUsername',
			new Route('/search-by-username/{username}', ['_controller' => 'Controllers\\IndexController::searchByUsername'])
		);

		$this->routes->add(
			'slideshow',
			new Route('/slideshow/{year}/{month}', ['_controller' => 'Controllers\\IndexController::slideshow'])
		);

		$this->routes->add(
			'getSlide',
			new Route('/get-slide', ['_controller' => 'Controllers\\IndexController::getSlide'])
		);

		$this->routes->add(
			'comments',
			new Route('/comments/photo_id', ['_controller' => 'Controllers\\CommentController::list'])
		);

		$this->routes->add(
			'saveComment',
			new Route('/save-comment', ['_controller' => 'Controllers\\CommentController::save'])
		);

		$this->routes->add(
			'like',
			new Route('/like/{photo_id}', ['_controller' => 'Controllers\\ToolsController::like'])
		);

		$this->routes->add(
			'translate',
			new Route('/translate', ['_controller' => 'Controllers\\ToolsController::translate'])
		);

		$this->routes->add(
			'switchLang',
			new Route('/switch-lang/{lang}/{date}', ['_controller' => 'Controllers\\IndexController::switchLang'])
		);

		$this->routes->add(
			'switchFr',
			new Route('/fr', ['_controller' => 'Controllers\\IndexController::switchFr'])
		);

		$this->routes->add(
			'contact',
			new Route('/contact', ['_controller' => 'Controllers\\IndexController::contact'])
		);

		$this->routes->add(
			'sendMessage',
			new Route('/send', ['_controller' => 'Controllers\\IndexController::sendMessage'])
		);

		$this->routes->add(
			'todayPhoto',
			new Route('/photo/today/{thumb}', ['_controller' => 'Controllers\\ToolsController::getTodayPhoto'])
		);

		$this->routes->add(
			'photo',
			new Route('/photo/{filename}/{extension}/{thumb}', ['_controller' => 'Controllers\\ToolsController::getPhoto'])
		);

		$this->routes->add(
			'download',
			new Route('/download/{id}', ['_controller' => 'Controllers\\ToolsController::download'])
		);

		$this->routes->add(
			'getFromNasa',
			new Route('/tools/get-from-nasa/{date}/{get_image}/{old}', ['_controller' => 'Controllers\\ToolsController::getFromNasa'])
		);

		$this->routes->add(
			'getNasaImages',
			new Route('/tools/get-nasa-images/{year}/{month}/{day}', ['_controller' => 'Controllers\\ToolsController::getNasaImages'])
		);

		$this->routes->add(
			'getNasaImage',
			new Route('/tools/get-nasa-image', ['_controller' => 'Controllers\\ToolsController::getNasaImage'])
		);

		$this->routes->add(
			'postSocial',
			new Route('/tools/post-social', ['_controller' => 'Controllers\\ToolsController::postSocial'])
		);

		$this->routes->add(
			'exportForm',
			new Route('/tools/export-form', ['_controller' => 'Controllers\\ToolsController::exportForm'])
		);

		$this->routes->add(
			'export',
			new Route('/tools/export/{year}/{month}/{day}', ['_controller' => 'Controllers\\ToolsController::export'])
		);

		$this->routes->add(
			'getTodayJson',
			new Route('/api/get-today-json', ['_controller' => 'Controllers\\ToolsController::getTodayJson'])
		);

		$this->routes->add(
			'populate',
			new Route('/tools/populate', ['_controller' => 'Controllers\\ToolsController::populate'])
		);

		$this->routes->add(
			'populateDates',
			new Route('/tools/populate-dates/{limit}', ['_controller' => 'Controllers\\ToolsController::populateDates'])
		);

		$this->routes->add(
			'uploadImage',
			new Route('/tools/upload-image', ['_controller' => 'Controllers\\ToolsController::uploadImage'])
		);

		$this->routes->add(
			'clearCache',
			new Route('/cache/clear', ['_controller' => 'Controllers\\ToolsController::clearCache'])
		);

		$this->routes->add(
			'sitemap',
			new Route('/sitemap.xml', ['_controller' => 'Controllers\\ToolsController::sitemap'])
		);

		$this->routes->add(
			'sitemapYear',
			new Route('/{year}/sitemap.xml', ['_controller' => 'Controllers\\ToolsController::sitemap'])
		);

		$this->routes->add(
			'checkLocalImages',
			new Route('/check-images/{year}', ['_controller' => 'Controllers\\ToolsController::checkImages'])
		);

		$this->routes->add(
			'rss',
			new Route('/rss/{page}/{limit}', ['_controller' => 'Controllers\\ToolsController::rss'])
		);

		$this->routes->add(
			'twitterDailyPost',
			new Route('/twitter-daily-post/{lang}', ['_controller' => 'Controllers\\TwitterController::dailyPost'])
		);

		$this->routes->add(
			'blueskyDailyPost',
			new Route('/bluesky-daily-post/{lang}', ['_controller' => 'Controllers\\BlueskyController::dailyPost'])
		);

		$this->routes->add(
			'mastodonDailyPost',
			new Route('/mastodon-daily-post/{lang}', ['_controller' => 'Controllers\\MastodonController::dailyPost'])
		);

		$this->routes->add(
			'tumnlrDailyPost',
			new Route('/tumblr-daily-post/{lang}', ['_controller' => 'Controllers\\TumblrController::dailyPost'])
		);

		$this->routes->add(
			'twitterCallback',
			new Route('/twitter-callback', ['_controller' => 'Controllers\\TwitterController::callback'])
		);

		$this->routes->add(
			'scroll',
			new Route('/scroll/{lang}', ['_controller' => 'Controllers\\IndexController::scroll'])
		);

		$this->routes->add(
			'getPreviousForScroll',
			new Route('/get-previous-for-scroll', ['_controller' => 'Controllers\\IndexController::getPreviousForScroll'])
		);
	}

	private function buildAdminRoutes()
	{
		$this->routes->add(
			'adminHome',
			new Route('/admin', ['_controller' => 'Controllers\\AdminController::home'])
		);

		$this->routes->add(
			'adminDashboard',
			new Route('/admin/dashboard', ['_controller' => 'Controllers\\AdminController::dashboard'])
		);

		$this->routes->add(
			'adminUsers',
			new Route('/admin/users', ['_controller' => 'Controllers\\UserController::list'])
		);

		$this->routes->add(
			'addUser',
			new Route('/admin/user/add', ['_controller' => 'Controllers\\UserController::form'])
		);

		$this->routes->add(
			'editUser',
			new Route('/admin/user/{id}', ['_controller' => 'Controllers\\UserController::form'])
		);

		$this->routes->add(
			'saveUser',
			new Route('/admin/save-user', ['_controller' => 'Controllers\\UserController::save'])
		);

		$this->routes->add(
			'deleteUser',
			new Route('/admin/delete-user/{id}', ['_controller' => 'Controllers\\UserController::delete'])
		);

		$this->routes->add(
			'adminCronExecutions',
			new Route('/admin/cron-executions/{page}/{sort_by}/{sort_order}', ['_controller' => 'Controllers\\CronController::list'])
		);

		$this->routes->add(
			'adminCronSearchForm',
			new Route('/admin/cron-search', ['_controller' => 'Controllers\\CronController::searchForm'])
		);

		$this->routes->add(
			'adminTranslations',
			new Route('/admin/translations', ['_controller' => 'Controllers\\TranslationController::list'])
		);

		$this->routes->add(
			'addTranslation',
			new Route('/admin/translation/add', ['_controller' => 'Controllers\\TranslationController::form'])
		);

		$this->routes->add(
			'editTranslation',
			new Route('/admin/translation/{id}', ['_controller' => 'Controllers\\TranslationController::form'])
		);

		$this->routes->add(
			'saveTranslation',
			new Route('/admin/save-translation', ['_controller' => 'Controllers\\TranslationController::save'])
		);

		$this->routes->add(
			'deleteTranslation',
			new Route('/admin/delete-translation/{id}', ['_controller' => 'Controllers\\TranslationController::delete'])
		);

		$this->routes->add(
			'adminPhotos',
			new Route('/admin/photos/{year}/{month}', ['_controller' => 'Controllers\\PhotoController::list'])
		);

		$this->routes->add(
			'addPhoto',
			new Route('/admin/photo/add/{year}/{month}', ['_controller' => 'Controllers\\PhotoController::form'])
		);

		$this->routes->add(
			'editPhoto',
			new Route('/admin/photo/{id}/edit/{translate}', ['_controller' => 'Controllers\\PhotoController::form'])
		);

		$this->routes->add(
			'savePhoto',
			new Route('/admin/save-photo', ['_controller' => 'Controllers\\PhotoController::save'])
		);

		$this->routes->add(
			'deletePhoto',
			new Route('/admin/delete-photo/{id}', ['_controller' => 'Controllers\\PhotoController::delete'])
		);

		$this->routes->add(
			'adminThumbnails',
			new Route('/admin/thumbnails/{year}', ['_controller' => 'Controllers\\ThumbnailController::list'])
		);

		$this->routes->add(
			'adminUploadThumbnail',
			new Route('/admin/upload-thumbnail', ['_controller' => 'Controllers\\ThumbnailController::upload'])
		);

		$this->routes->add(
			'adminGenerateThumbnail',
			new Route('/admin/generate-thumbnail/{year}/{month}/{nb_lines}', ['_controller' => 'Controllers\\ThumbnailController::generate'])
		);

		$this->routes->add(
			'adminMosaics',
			new Route('/admin/mosaics/{year}', ['_controller' => 'Controllers\\MosaicController::list'])
		);

		$this->routes->add(
			'adminGenerateMosaic',
			new Route('/admin/generate-mosaic/{year}', ['_controller' => 'Controllers\\MosaicController::generate'])
		);

		$this->routes->add(
			'adminTags',
			new Route('/admin/tags', ['_controller' => 'Controllers\\TagController::list'])
		);

		$this->routes->add(
			'addTag',
			new Route('/admin/tag/add', ['_controller' => 'Controllers\\TagController::form'])
		);

		$this->routes->add(
			'editTag',
			new Route('/admin/tag/{id}', ['_controller' => 'Controllers\\TagController::form'])
		);

		$this->routes->add(
			'saveTag',
			new Route('/admin/save-tag', ['_controller' => 'Controllers\\TagController::save'])
		);

		$this->routes->add(
			'deleteTag',
			new Route('/admin/delete-tag/{id}', ['_controller' => 'Controllers\\TagController::delete'])
		);

		$this->routes->add(
			'populateTag',
			new Route('/admin/populate-tag/{id}/{reset}', ['_controller' => 'Controllers\\TagController::populate'])
		);

		$this->routes->add(
			'populateCountryTag',
			new Route('/admin/populate-country-tag/{letter}', ['_controller' => 'Controllers\\TagController::populateCountry'])
		);

		$this->routes->add(
			'adminYears',
			new Route('/admin/years', ['_controller' => 'Controllers\\YearController::list'])
		);

		$this->routes->add(
			'addYear',
			new Route('/admin/year/add', ['_controller' => 'Controllers\\YearController::form'])
		);

		$this->routes->add(
			'editYear',
			new Route('/admin/year/{id}', ['_controller' => 'Controllers\\YearController::form'])
		);

		$this->routes->add(
			'saveYear',
			new Route('/admin/save-year', ['_controller' => 'Controllers\\YearController::save'])
		);

		$this->routes->add(
			'deleteYear',
			new Route('/admin/delete-year/{id}', ['_controller' => 'Controllers\\YearController::delete'])
		);

		$this->routes->add(
			'adminBackupList',
			new Route('/admin/backups', ['_controller' => 'Controllers\\AdminController::backupList'])
		);

		$this->routes->add(
			'adminBackupDownload',
			new Route('/admin/backup/{date}/download', ['_controller' => 'Controllers\\AdminController::backupDownload'])
		);

		$this->routes->add(
			'adminBackupCreate',
			new Route('/admin/backup/create', ['_controller' => 'Controllers\\AdminController::backupCreate'])
		);

		$this->routes->add(
			'adminComments',
			new Route('/admin/comments/{photo_id}', ['_controller' => 'Controllers\\CommentController::admin'])
		);

		$this->routes->add(
			'adminEditComment',
			new Route('/admin/comment/{id}', ['_controller' => 'Controllers\\CommentController::form'])
		);

		$this->routes->add(
			'adminDeleteComment',
			new Route('/admin/delete-comment/{id}', ['_controller' => 'Controllers\\CommentController::delete'])
		);

		$this->routes->add(
			'adminSaveComment',
			new Route('/admin/delete-comment/{id}', ['_controller' => 'Controllers\\CommentController::save'])
		);
	}
}
