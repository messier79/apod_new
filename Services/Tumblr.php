<?php

namespace Services;

use Config\Config;
use Repositories\PhotoRepository;
use Tumblr\API\Client;

class Tumblr
{
	/**
	 * @param string $lang
	 * @return void
	 */
	public static function post(string $lang)
	{
		$date = new \DateTime();
		$config         = new Config();
		$baseUrl        = $config->getParameter('base_url');
		$blogName       = $config->getParameter('tumblr_blog_name');
		$consumerKey    = $config->getParameter('tumblr_consumer_key');
		$consumerSecret = $config->getParameter('tumblr_consumer_secret');
		$token          = $config->getParameter('tumblr_token');
		$tokenSecret    = $config->getParameter('tumblr_token_secret');

		if ($lang === 'fr') {
			$parser  = new \IntlDateFormatter(
				'fr_FR',
				\IntlDateFormatter::FULL,
				\IntlDateFormatter::FULL,
				'America/New_York',
				\IntlDateFormatter::GREGORIAN,
				'dd LLLL yyyy'
			);
			$dateStr = $parser->format($date);
		} else {
			$dateStr = date("F j, Y", $date->getTimestamp());
		}

		$repo      = new PhotoRepository();
		$photo     = $repo->findLatest();
		$fileParts = preg_split('/\./', $photo->getProperty('filename'));
		$thumbUrl  = $baseUrl . 'photo/' . $fileParts[0] . '/' . $fileParts[1];
		$url       = $baseUrl . $photo->getProperty('url') . '/' . $lang;
		$data      = [
			'type'        => 'link',
			'url'         => $url,
			'thumbnail'   => $thumbUrl,
			'description' => $dateStr . ' - ' . $photo->getProperty('title_' . $lang),
		];

		$client = new Client($consumerKey, $consumerSecret);
		$client->setToken($token, $tokenSecret);

		$client->createPost($blogName, $data);
	}
}