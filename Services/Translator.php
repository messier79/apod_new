<?php

namespace Services;

class Translator
{
	/**
	 * @param string $text
	 * @param string $langFrom
	 * @param string $langTo
	 * @return false|\stdClass
	 */
	public static function translate(string $text, string $langFrom = 'en', string $langTo = 'fr')
	{
		try {
			$text    = urlencode($text);
			$result = new \stdClass();
			$result->original = $text;
			$result->source = $langFrom;
			$result->target = $langTo;
			$result->translation = 'No library';

			return $result;
		} catch (\Exception $e) {
			echo $e->getMessage();

			return false;
		}
	}
}
