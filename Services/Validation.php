<?php

namespace Services;

use Helpers\Tools;

class Validation
{
	public static function validateClassParameters(string $className, array $fields): array
	{
		$class             = new \ReflectionClass($className); // get class object
		$properties        = $class->getProperties(); // get class properties
		$errors = [];

		foreach ($properties as $property) {
			// skip inherited properties
			if ($property->getDeclaringClass()->getName() !== $class->getName()) {
				continue;
			}

			$propertyName = $property->getName();
			$comments = $property->getDocComment();
			$annotations = Tools::extractAnnotationsFromComments($comments);

			if (isset($annotations['@ignore'])) {
				continue;
			}

			if (!isset($fields[$propertyName]) && !isset($annotations['@optional'])) {
				$errors[$propertyName] = 'mandatory';
			} elseif (isset($fields[$propertyName])) {
				foreach ($annotations['@var']['types'] as $type) {
					$errorFound = false;

					switch ($type) {
						case 'int':
							if (!self::isInt($fields[$propertyName])
								&& !self::isNewId($propertyName, $fields[$propertyName])
							) {
								echo $propertyName . ' ' . is_null($fields[$propertyName])  .'.';
								$errors[$propertyName] = [
									'message' => 'wrong_type_int'
								];
								$errorFound = true;
							}
							break;
						case 'float':
							if (!self::isFloat($fields[$propertyName])) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_float'
								];
								$errorFound = true;
							}
							break;
						case 'double':
							if (!self::isDouble($fields[$propertyName])) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_double'
								];
								$errorFound = true;
							}
							break;
						case 'array':
							if (!is_array($fields[$propertyName])) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_array'
								];
								$errorFound = true;
							}
							break;
						case 'string':
							if (!isset($annotations['@optional'])
								&& $fields[$propertyName] === ''
							) {
								$errors[$propertyName] = [
									'message' => 'mandatory'
								];
								$errorFound = true;
							} elseif (!is_string($fields[$propertyName])) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_string'
								];
								$errorFound = true;
							} else {
								if (isset($annotations['@format'])) {
									$options = preg_split('/,/', $annotations['@format']['options']);

									foreach ($options as $option) {
										$optionParams = preg_split('/=/', $option);
										$key = $optionParams[0];

										if (isset($optionParams[1])) {
											$val = $optionParams[1];
										}

										if ($key === 'max_length'
											&& strlen($fields[$propertyName]) > $val
										) {
											$errors[$propertyName] = [
												'message' => 'wrong_string',
												'max_length' > $val,
											];
											$errorFound            = true;
										} elseif ($key === 'min_length'
											&& strlen($fields[$propertyName]) < $val
										) {
											$errors[$propertyName] = [
												'message' => 'wrong_string',
												'min_length' > $val,
											];
											$errorFound            = true;
										} elseif ($key === 'exact_length'
											&& strlen($fields[$propertyName]) !== $val
										) {
											$errors[$propertyName] = [
												'message' => 'wrong_string',
												'exact_length' > $val,
											];
											$errorFound            = true;
										} elseif ($key === 'email'
											&& !self::isValidEmail($fields[$propertyName])
										) {
											$errors[$propertyName] = [
												'message' => 'wrong_email',
											];
											$errorFound            = true;
										}
									}
								}
							}
							break;
					}

					if ($errorFound === false) {
						if (substr($type, 0, 8) === 'DateTime') {
							if (isset($annotations['@format']['options'])) {
								$format = $annotations['@format']['options'];
							} else {
								$format = 'Y-m-d H:i:s';
							}
							$date = \DateTime::createFromFormat($format, $fields[$propertyName]);

							if ($date === false || array_sum($date::getLastErrors()) > 0) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_date',
									'expected_format' => $format,
								];
								$errorFound = true;
							}
						}
					}

					if ($errorFound === false) {
						if (substr($type, 0, 4) === 'enum') {
							$parts = preg_split('/\|/', $type);
							$values = preg_split('/,/', $parts[1]);

							if (!in_array($fields[$propertyName], $values)) {
								$errors[$propertyName] = [
									'message' => 'wrong_type_enum',
									'expected_values' => $parts[1],
								];
								$errorFound = true;
							}
						}
					}
				}
			}
		}

		return $errors;
	}

	/**
	 * @param string $value
	 * @return bool
	 */
	public static function isInt(string $value): bool
	{
		if (!is_numeric($value) && !is_int($value)) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $value
	 * @return bool
	 */
	public static function isFloat(string $value): bool
	{
		if (!self::isInt($value) && !is_float($value)) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $value
	 * @return bool
	 */
	public static function isDouble(string $value): bool
	{
		if (!self::isFloat($value) && !is_double($value)) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $email
	 * @return bool
	 */
	public static function isValidEmail(string $email): bool
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	/**
	 * @param string $propertyName
	 * @param mixed $value
	 * @return bool
	 */
	private static function isNewId(string $propertyName, $value): bool
	{
		if ($propertyName === 'id' && $value === null) {
			return true;
		} else {
			return false;
		}
	}
}