<?php

namespace Services;

use Config\Config;
use Helpers\Image;
use Models\Photo as PhotoEntity;
use Repositories\PhotoRepository;
use Symfony\Component\HttpFoundation\Request;

class Photo
{
	public static function getTodayPhoto()
	{
		$today = new \DateTime();
		$repo  = new PhotoRepository();

		return $repo->findByDate($today);
	}

	public static function getLatest()
	{
		$today = new \DateTime();
		$repo  = new PhotoRepository();

		return $repo->findLatest();
	}

	public static function getPhoto($date)
	{
		$repo = new PhotoRepository();

		return $repo->findByDate($date);
	}

	public static function search(array $params, int $page)
	{
		$repo = new PhotoRepository();

		if (isset($params['username'])) {
			return $repo->findByCommentUsername($params, $page);
		} else {
			return $repo->search($params, $page);
		}
	}

	public static function getPreviousDay(string $dateStr)
	{
		$date = \DateTime::createFromFormat('ymd', $dateStr);
		$date->modify('-1 day');

		return $date;
	}

	public static function getNextDay(string $dateStr)
	{
		$date = \DateTime::createFromFormat('ymd', $dateStr);
		$date->modify('+1 day');

		return $date;
	}

	/**
	 * @return \Models\Photo|null
	 */
	public static function getRandom()
	{
		$repo = new PhotoRepository();

		return $repo->getRandom();
	}

	/**
	 * @param string $filename
	 * @return string
	 */
	public static function getDateFromFilename(string $filename): string
	{
		$tmp       = preg_split('/\./', $filename);
		$dateStr   = $tmp[0];
		$yearShort = substr($dateStr, 0, 2);
		$month     = (int)substr($dateStr, 2, 2);
		$day       = (int)substr($dateStr, 4, 2);

		if ($yearShort > 90) {
			$year = '19' . $yearShort;
		} else {
			$year = '20' . $yearShort;
		}

		if ($month < 10) {
			$month = '0' . $month;
		}

		if ($day < 10) {
			$day = '0' . $day;
		}

		return $year . '-' . $month . '-' . $day;
	}

	/**
	 * @param int $year
	 * @param int $month
	 * @param int $nbLines
	 * @return bool
	 */
	public static function generateMonthThumbnail(int $year, int $month, int $nbLines = 5): bool
	{
		if ($month < 10) {
			$month = '0' . $month;
		}

		$filename        = $year . $month . '.png';
		$sourcePath      = __DIR__ . '/../storage/' . $filename;

		if (file_exists($sourcePath)) {
			$destinationPath = __DIR__ . '/../Resources/assets/images/months/' . $filename;
			$height          = Image::MONTH_THUMBNAIL_CROP_HEIGHT_5;

			if ($nbLines === 4) {
				$height = Image::MONTH_THUMBNAIL_CROP_HEIGHT_4;
			} elseif ($nbLines === 6) {
				$height = Image::MONTH_THUMBNAIL_CROP_HEIGHT_6;
			}

			Image::crop(
				$sourcePath,
				$destinationPath,
				Image::MONTH_THUMBNAIL_CROP_WIDTH,
				$height,
				Image::MONTH_THUMBNAIL_CROP_X,
				Image::MONTH_THUMBNAIL_CROP_Y
			);
			Image::generateImageThumbnail(
				$destinationPath,
				$destinationPath,
				Image::MONTH_THUMBNAIL_WIDTH,
				Image::MONTH_THUMBNAIL_WIDTH
			);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param int $year
	 * @return bool
	 */
	public static function generateYearMosaic(int $year): bool
	{
		$filename        = $year . '.png';
		$sourcePath      = __DIR__ . '/../storage/' . $filename;

		if (file_exists($sourcePath)) {
			$destinationPath = __DIR__ . '/../Resources/assets/images/mosaics/' . $filename;
			$height          = Image::YEAR_MOSAIC_CROP_HEIGHT;

			Image::crop(
				$sourcePath,
				$destinationPath,
				Image::YEAR_MOSAIC_CROP_WIDTH,
				$height,
				Image::YEAR_MOSAIC_CROP_X,
				Image::YEAR_MOSAIC_CROP_Y
			);

			return true;
		} else {
			return false;
		}
	}

	public static function listPendingThumbnails()
	{
		$dir = __DIR__ . '/../storage/';
		$pending = [];

		foreach(glob($dir . '*.{jpg,JPG,jpeg,JPEG,png,PNG}',GLOB_BRACE) as $file){
			$tmp = preg_split('/\./', basename($file));
			$pending[$tmp[0]] = true;
		}

		return $pending;
	}

	/**
	 * @param int $photoId
	 * @return string
	 */
	public static function like(int $photoId)
	{
		$cookieName = 'like_' . $photoId;

		if (!isset($_COOKIE[$cookieName])) {
			$repo = new PhotoRepository();
			$repo->like($photoId);

			setcookie($cookieName, 'true', strtotime("+1 year"));

			return 'true';
		} else {
			return 'false';
		}
	}

	/**
	 * @param string $filename
	 * @return bool
	 */
	public static function checkCdn(string $filename)
	{
		if (substr($filename, 0, 1) !== '9') {
			$config = new Config();
			$cdnUrl = $config->getParameter('cdn_url');
			$url    = $cdnUrl . '20' . substr($filename, 0, 2) . '/' . $filename;
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

			/* Get the HTML or whatever is linked in $url. */
			$response = curl_exec($handle);

			/* Check for 404 (file not found). */
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

			if ($httpCode !== 404) {
				return true;
			}
		}

		return false;
	}

	public static function download(int $photoId)
	{
		$config      = new Config();
		$photo = new PhotoEntity($photoId);
		$hasCdn = Photo::checkCdn($photo->getProperty('filename'));
		$parts = preg_split('/\./', $photo->getProperty('filename'));
		$extension = $parts[count($parts) - 1];
		$filename = substr(
			$photo->getProperty('filename'),
			0,
			strlen($photo->getProperty('filename')) - (strlen($extension) + 1)
		);
		$year = substr($photo->getProperty('filename'), 0, 2);

		if ($hasCdn) {
			$cdnUrl      = $config->getParameter('cdn_url');
			$url = $cdnUrl . '20' . $year . '/' . $filename . '.' . $extension;
		} else {
			$baseUrl     = $config->getParameter('base_url');
			$url = $baseUrl . 'photo/' . $filename . '/' . $extension;
		}

		$date = new \DateTime($photo->getProperty('date'));
		$filename = $date->format('Y-m-d') .
			'-' .
			strtolower(preg_replace('/ /', '-', $photo->getProperty('title_en')));

		header('Content-Type: application/x-file-to-save');
		header("Content-disposition: attachment; filename=\"" . $filename . '.' . $extension . "\"");
		ob_end_clean();
		readfile($url);
	}

	public static function findForSlideShow(Request $request)
	{
		if ($request->request->has('random')) {
			$photo = Photo::getRandom();
			$date  = new \DateTime($photo->getProperty('date'));
		} else {
			$repo = new PhotoRepository();
			$date = \DateTime::createFromFormat('ymd', $request->get('current'));
			$date->setTime(0, 0, 0);
			$year = $request->get('year');
			$month = '01';
			$firstDate = new \DateTime($year . '-01-01');
			$lastDate = new \DateTime($year . '-12-31');

			if ($request->request->has('month')) {
				$month = (int)$request->get('month');
				$month = str_pad($month, 2, '0', STR_PAD_LEFT);
				$lastDate = new \DateTime($year . '-' . $month . '-01');
				$lastDate->setTime(0, 0, 0);
				$firstDate = clone $lastDate;
				$lastDate->modify('last day of this month');
			}

			if ($request->get('type') === 'previous') {
				$date->modify("-1 day");
			} else {
				$date->modify("+1 day");
			}

			if ($date > $lastDate) {
				$date = $firstDate;
			} elseif ($date < $firstDate) {
				$date = $lastDate;
			}

			$photo = $repo->findByDate($date);

			if ($photo === null) {
				$dateStr = $year;

				if ($request->request->has('month')) {
					$dateStr .= '-' . $month;
				}

				$dateStr .= '-01';
				$date    = new \DateTime($dateStr);
				$photo   = $repo->findByDate($date);
			}
		}

		return [
			'photo' => $photo,
			'date'  => $date,
		];
	}
}
