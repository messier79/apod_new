<?php

namespace Services;

use Config\Config;
use Helpers\Image;
use Repositories\PhotoRepository;
use Repositories\YearRepository;
use voku\helper\HtmlDomParser;

class Nasa
{
	const URL = 'https://apod.nasa.gov/apod/';

	public static function getInfoByDate(string $date, bool $getImage = true, $old = false)
	{
		error_reporting(E_ERROR | E_PARSE);

		$dateObj  = \DateTime::createFromFormat('ymd', $date);
		$photoUrl = null;

		try {
			$today   = new \DateTime();

			if ($dateObj > $today) {
				die('Error');
			}

			$url      = self::URL . 'ap' . $date . '.html';
			$options = [
				CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
				CURLOPT_POST           =>false,        //set to GET
				CURLOPT_RETURNTRANSFER => true,     // return web page
				CURLOPT_FOLLOWLOCATION => true,     // follow redirects
				CURLOPT_HEADER         => false,    // don't return headers
				CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
				CURLOPT_TIMEOUT        => 120,      // timeout on response
			];

			$ch      = curl_init( $url );
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt_array( $ch, $options );
			$content = curl_exec( $ch );
			$err     = curl_errno( $ch );
			$errmsg  = curl_error( $ch );
			$header  = curl_getinfo( $ch );
			curl_close( $ch );

			$dom      = HtmlDomParser::str_get_html($content);
			$video    = '';
			$filename = $date . '.jpg';

			// Checks if has embedded content
			$iframe = $dom->findOneOrFalse('iframe');

			if ($iframe !== false) {
				$src = preg_split('/\//', $iframe->src);

				if (count($src) === 1) {
					die('Error src');
				}
				$pos   = count($src) - 1;
				$tmp   = preg_split('/\?/', $src[$pos]);
				$video = $tmp[0];
			} else {
				$photoLink = $dom->findMulti('a');
				
				if ($old === true) {
					$photoHref = $photoLink[0]->href;
				} else {
					$photoHref = $photoLink[1]->href;
				}
				$fileParts = preg_split('/\./', $photoHref);
				$photoUrl  = self::URL . $photoHref;
				$filename  = $date . '.' . $fileParts[1];

				// Download file and creates thumbnail
				if ($getImage === true) {
					$config        = new Config();
					$photoPath     = $config->getParameter('photos_directory')
						. $dateObj->format('Y') . '/';
					$thumbnailPath = $config->getParameter('photos_directory')
						. $dateObj->format('Y') . '/'
						. '_sfpg_data/thumb/';

					if (!is_dir($photoPath)) {
						mkdir($photoPath, 0775, true);
					}

					if (!is_dir($thumbnailPath)) {
						mkdir($thumbnailPath, 0775, true);
					}

					$photoPath     .= $filename;
					$thumbnailPath .= $filename;

					Image::downloadImage($photoUrl, $photoPath, $date);
					Image::resize($photoPath, $photoPath, 1024, 1024);
					Image::resize($photoPath, $thumbnailPath, 160, 160);
				}
			}

			if ($old === true) {
				$bContent = $dom->findMulti('b');
				$title    = $bContent[0]->innerhtml;
				$credits  = $bContent[1]->innerhtml;
			} else {
				$centeredContent = $dom->findMulti('center');
				$parts           = preg_split('/<br>/', $centeredContent[1]->innerhtml);
				$title           = $parts[0];
				$credits         = $parts[1];
			}
			$title           = preg_replace('/<b>/', '', $title);
			$title           = preg_replace('/<\/b>/', '', $title);
			$title           = trim($title);
			$title           = preg_replace('/\r|\n/', '', $title);
			$credits         = preg_replace('/<b>/', '', $credits);
			$credits         = preg_replace('/<\/b>/', '', $credits);
			$credits         = trim($credits);

			$content     = $dom->innerHtml;
			$parts       = preg_split('/<b> Explanation: <\/b>/', $content);
			$description = '';

			if (count($parts) > 1) {
				$tmp         = preg_split('/<\/p>/', $parts[1]);
				$description = trim($tmp[0]);
				$description = preg_replace('/ap(.+)\.html/', '/day/$1', $description);
			}

//echo $title . '<br>';
//echo $credits . '<br>';
//echo $description . '<br>';die();
			$photoRepo = new PhotoRepository();
			$photo     = $photoRepo->findByDate($dateObj);

			if ($photo === null) {
				$yearRepo = new YearRepository();
				$year     = $yearRepo->findByName($dateObj->format('Y'));
				$dateStr = Photo::getDateFromFilename($filename);
				$photo = new \Models\Photo();
				$photo->setProperty('year_id', $year->getId())
					->setProperty('date', $dateStr)
					->setProperty('filename', $filename)
					->setProperty('video', $video);
			}

			$photo->setProperty('title_en', $title)
				->setProperty('description_en', $description)
				->setProperty('image_url', $photoUrl)
				->setProperty('credits', $credits)
				->setProperty('status', 'enabled')
				->setProperty('likes', 0)
				->setProperty('created_at', date('Y-m-d H:i:s'))
				->save();

			Tag::addPhotoTagsFromDescription($photo);
		} catch (\Exception $e) {
			echo $e->getMessage() . '<br>';
			echo $e->getFile() . '<br>';
			echo $e->getLine() . '<br>';
		}

		$dateObj->add(new \DateInterval('P1D'));
		
		$url = 'https://apod.vidry.ca/tools/get-from-nasa/' . $dateObj->format('ymd');
		
		if ($getImage) {
			$url .= '/true';
		} else {
			$url .= '/false';
		}
		
		if ($old) {
			$url .= '/true';
		} else {
			$url .= '/false';
		}
		
		echo '<a href="' . $url . '">' . $dateObj->format('ymd') . '</a>';
	}


	public static function getImagesByDate(int $year, int $month, int $day, int $limit = 0)
	{
		error_reporting(E_ERROR | E_PARSE);

		$month   = str_pad($month, 2, '0', STR_PAD_LEFT);;
		$day     = str_pad($day, 2, '0', STR_PAD_LEFT);;
		$dateStr = $year . '-' . $month . '-' . $day;
		$dateObj = new \DateTime($dateStr);
		$nb      = 0;

		try {
			while($dateObj->format('m') === $month) {
				$today = new \DateTime();

				if ($dateObj > $today) {
					die('End');
				}

				$date = $dateObj->format('ymd');
				$url     = self::URL . 'ap' . $date . '.html';
				$options = [
					CURLOPT_CUSTOMREQUEST  => "GET",        //set request type post or get
					CURLOPT_POST           => false,        //set to GET
					CURLOPT_RETURNTRANSFER => true,     // return web page
					CURLOPT_FOLLOWLOCATION => true,     // follow redirects
					CURLOPT_HEADER         => false,    // don't return headers
					CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
					CURLOPT_TIMEOUT        => 120,      // timeout on response
				];

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt_array($ch, $options);
				$content = curl_exec($ch);
				curl_close($ch);

				$dom      = HtmlDomParser::str_get_html($content);

				// Checks if has embedded content
				$iframe = $dom->findOneOrFalse('iframe');

				if ($iframe !== false) {
					$src = preg_split('/\//', $iframe->src);

					if (count($src) === 1) {
						die('Error src');
					}
					$pos   = count($src) - 1;
					$tmp   = preg_split('/\?/', $src[$pos]);
					$video = $tmp[0];
				} else {
					$photoLink = $dom->findMulti('a');
					$photoHref = $photoLink[1]->href;
					$fileParts = preg_split('/\./', $photoHref);
					$photoUrl  = self::URL . $photoHref;
					$filename  = $date . '.' . $fileParts[1];

					// Download file and creates thumbnail
					$config        = new Config();
					$photoPath     = $config->getParameter('photos_directory')
						. $dateObj->format('Y') . '/';
					$thumbnailPath = $config->getParameter('photos_directory')
						. $dateObj->format('Y') . '/'
						. '_sfpg_data/thumb/';

					if (!is_dir($photoPath)) {
						mkdir($photoPath, 0775, true);
					}

					$photoPath .= $filename;

					echo $photoPath . '<br>';
					Image::downloadImage($photoUrl, $photoPath, $date);
				}


				echo $dateObj->format('ymd') . '<br>';

				$nb++;

				if ($limit > 0 && $nb >= $limit) {
					break;
				}

				$dateObj->modify('+1 day');
			}
		} catch (\Exception $e) {
			echo $e->getMessage() . '<br>';
			echo $e->getFile() . '<br>';
			echo $e->getLine() . '<br>';
		}
	}
}
