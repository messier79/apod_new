<?php

namespace Services;

use Config\Config;

class Mastodon
{
	/**
	 * @param string $content
	 * @return mixed
	 */
	public static function post(string $content)
	{
		$config        = new Config();
		$mastodonUrl   = $config->getParameter('mastodon_url');
		$mastodonToken = $config->getParameter('mastodon_token');

		// the main status update array, this will have media IDs added to it further down
		// and will be used when you send the main status update using steps in the first article
		$status_data = [
			"status"     => $content,
			"language"   => "eng",
			"visibility" => "public",
		];

		$headers = [
			'Authorization: Bearer ' . $mastodonToken,
		];

		// send the image using a cURL POST
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $mastodonUrl . "api/v1/statuses");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $status_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_exec($ch);

		$output = curl_getinfo($ch);

		echo curl_error($ch);
		curl_close ($ch);

		return $output;
	}
}