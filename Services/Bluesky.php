<?php

namespace Services;

use cjrasmussen\BlueskyApi\BlueskyApi;
use Config\Config;

class Bluesky
{
	/**
	 * @param string $content
	 * @param string $url
	 * @return mixed|object|string|null
	 */
	public static function post(string $content, string $filename, string $url)
	{
		$config    = new Config();
		$start     = strlen($content);
		$end       = $start + strlen($url) + 1;
		$tmp       = explode('.', $filename);
		$extension = $tmp[count($tmp) - 1];

		$username = $config->getParameter('bluesky_username');
		$password = $config->getParameter('bluesky_password');

		if ($username !== '') {
			$bluesky = new BlueskyApi();

			try {
				$bluesky->auth($username, $password);

				$body     = file_get_contents($config->getParameter('photos_directory') . date('Y') . '/' . $filename);
				$response = $bluesky->request('POST', 'com.atproto.repo.uploadBlob', [], $body, 'image/'.$extension);
				$image    = $response->blob;

				$args     = [
					'collection' => 'app.bsky.feed.post',
					'repo'       => $bluesky->getAccountDid(),
					'record'     => [
						'text'      => $content . ' ' . $url,
						'langs'     => ['en'],
						'createdAt' => date('c'),
						'$type'     => 'app.bsky.feed.post',
						'facets'    => [
							[
								'index'    => [
									'byteStart' => $start,
									'byteEnd'   => $end,
								],
								'features' => [
									[
										'$type' => 'app.bsky.richtext.facet#link',
										'uri'   => $url,
									],
								],
							],
						],
						'embed'     => [
							'$type'  => 'app.bsky.embed.images',
							'images' => [
								[
									'alt'   => 'APOD',
									'image' => $image,
								],
							],
						],],
				];
				$response = $bluesky->request('POST', 'com.atproto.repo.createRecord', $args);
			} catch (\Exception $e) {
				echo $e->getMessage();

				$response = 'Connection error';
			}
		} else {
			$response = 'No API Key';
		}

		return $response;
	}
}