<?php

namespace Services;

use Config\Config;
use Symfony\Component\HttpFoundation\Request;

class HCaptcha
{
	/**
	 * @param Request $request
	 * @return bool
	 */
	public static function check(Request $request): bool
	{
		$config   = new Config();
		$hCaptcha = $config->getParameter('h_captcha');
		$data     = [
			'secret'   => $hCaptcha['SECRET'],
			'response' => $request->get('h-captcha-response'),
		];
		$verify   = curl_init();
		curl_setopt($verify, CURLOPT_URL, $hCaptcha['URL']);
		curl_setopt($verify, CURLOPT_POST, true);
		curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
		$response     = curl_exec($verify);
		$responseData = json_decode($response);
		curl_close($verify);

		if ($responseData !== null && $responseData->success) {
			return true;
		} else {
			return false;
		}
	}
}