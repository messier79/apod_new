<?php

namespace Services;

use Config\Config;
use Helpers\Session;
use Repositories\TranslationRepository;

class Translation
{
	public static function findByLang(string $lang = null): array
	{
		$repo = new TranslationRepository();

		if ($lang === null) {
			if (Session::has('lang')) {
				$lang = Session::get('lang');
			} else {
				$config = new Config();
				$lang = $config->getParameters('default_lang');
			}
		}
		
		return $repo->findByLang($lang);
	}
}