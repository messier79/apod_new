<?php

namespace Services;

use Models\Photo as PhotoEntity;
use Models\Tag as TagEntity;
use Repositories\PhotoRepository;
use Repositories\TagRepository;

class Tag
{
	/**
	 * @param PhotoEntity $photo
	 * @param array $tags
	 */
	public static function addPhotoTags(PhotoEntity &$photo, array $tags)
	{
		$tagRepo = new TagRepository();

		$tagRepo->deletePhotoTags($photo->getId());

		foreach ($tags as $tag) {
			$tagRepo->insertPhotoTag($photo->getId(), $tag);
		}
	}

	/**
	 * @param PhotoEntity $photo
	 * @param array $tags
	 */
	public static function addPhotoTagsFromDescription(PhotoEntity &$photo)
	{
		$tagRepo = new TagRepository();
		$tags    = $tagRepo->fetchAll();

		$tagRepo->deletePhotoTags($photo->getId());

		foreach ($tags as $tag) {
			$description = $photo->getProperty('description_en');

			if (preg_match('/' . $tag->getProperty('tag') . '/', $description) === 1) {
				$tagRepo->insertPhotoTag($photo->getId(), $tag->getId());
			}
		}
	}

	public static function populate(TagEntity $tag, bool $reset = false)
	{
		$photoRepo = new PhotoRepository();
		$tagRepo = new TagRepository();
		$photos = $photoRepo->findByTagInDescription($tag);

		if ($reset) {
			$tagRepo->deleteTagPhotos($tag->getId());
		}

		foreach ($photos as $photo) {
			$tagRepo->insertPhotoTag($photo['id'], $tag->getId());
		}
	}
}
