<?php

namespace Controllers;

use Helpers\Views;
use Models\Year;
use Repositories\YearRepository;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class YearController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		$sortBy                        = $request->request->has('sort_by') ? $request->get('sort_by') : 'name';
		$sortOrder                     = $request->request->has('sort_order') ? $request->get('sort_order') : 'ASC';
		$params                        = [
			'sortBy'    => $sortBy,
			'sortOrder' => $sortOrder,
		];
		$repo                          = new YearRepository();
		$this->viewParameters['years'] = $repo->fetchAll($params);

		Views::display('admin/years/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function form(Request $request)
	{
		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$year = new Year((int)$request->get('id'));

			if ((int)$request->get('id') === 0) {
				$year->setProperty('id', $request->get('id'))
					->setProperty('directory', $request->get('directory'))
					->setProperty('name', $request->get('name'))
					->setProperty('cover', $request->get('cover'))
					->setProperty('date', $request->get('date'))
					->setProperty('status', $request->get('status'));
			}
		} else {
			$year = new Year();
			$year->setProperty('id', 0)
				->setProperty('directory', '')
				->setProperty('name', '')
				->setProperty('cover', '')
				->setProperty('date', '')
				->setProperty('status', '');
		}
		$this->viewParameters['year'] = $year;

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		Views::display('admin/years/form', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(Year::class, $request->request->all());

		if (count($errors) === 0) {
			$id   = (int)$request->get('id');
			$date = new \DateTime();
			$year = new Year($id);
			$year->setProperty('directory', $request->get('directory'))
				 ->setProperty('name', $request->get('name'))
				 ->setProperty('cover', $request->get('cover'))
				 ->setProperty('date', $request->get('date'))
				 ->setProperty('status', $request->get('status'))
				->setProperty('updated_at', $date->format('Y-m-d H:i'));

			if ($id === 0) {
				$year->setProperty('created_at', $date->format('Y-m-d H:i'));
			}

			if (count($errors) === 0) {
				$year->save();
			}
		}

		if (count($errors) === 0) {
			$request->request->set('success', ['saved']);
			$this->list($request);
		} else {
			$request->request->set('errors', $errors);
			$this->form($request);
		}
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function delete(Request $request)
	{
		$year = new Year((int)$request->get('id'));
		$year->setProperty('deleted_at', new \DateTime())
			->save();

		$this->list($request);
	}
}
