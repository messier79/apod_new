<?php

namespace Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use Config\Config;
use Helpers\Tools;
use Symfony\Component\HttpFoundation\Request;

class TwitterController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct(bool $fromCron = false)
	{
		parent::__construct();
	}

	public function dailyPost(Request $request)
	{
		if ($request->request->has('lang')) {
			$lang = $request->get('lang');
		} else {
			$lang = 'en';
		}

		$config            = new Config();
		$baseUrl           = $config->getParameter('base_url');
		$consumerKey       = $config->getParameter('consumer_key');
		$consumerSecret    = $config->getParameter('consumer_secret');
		$accessToken       = $config->getParameter('access_token');
		$accessTokenSecret = $config->getParameter('access_token_secret');

		$twitter = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
		$twitter->setApiVersion('2');
		$url     = $baseUrl . 'day/' . date('ymd') . '/' . $lang;

		if ($lang === 'fr') {
			$content = '#imageastrodujour #apod ';
		} else {
			$content = '#astronomypictureoftheday #apod ';
		}

		$content .= $url;

		Tools::log('Daily Twitter cron (apod.vidry.ca) - ' . $lang);

		return $twitter->post("tweets", ["text" => $content], true);
	}

	public function callback(Request $request)
	{
		exit;
	}
}
