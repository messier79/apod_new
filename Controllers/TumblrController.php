<?php

namespace Controllers;

use Helpers\Tools;
use Services\Tumblr;
use Symfony\Component\HttpFoundation\Request;

class TumblrController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct(bool $fromCron = false)
	{
		parent::__construct();
	}

	/**
	 * @param Request $request
	 */
	public function dailyPost(Request $request)
	{
		if ($request->request->has('lang')) {
			$lang = $request->get('lang');
		} else {
			$lang = 'en';
		}

		Tools::log('Daily Tumblr cron (apod.vidry.ca) - ' . $lang);

		Tumblr::post($lang);
	}
}
