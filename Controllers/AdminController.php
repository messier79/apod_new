<?php

namespace Controllers;

use Config\Config;
use Helpers\Db;
use Helpers\File;
use Helpers\Views;
use Repositories\CronExecutionRepository;
use Repositories\PhotoRepository;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
		
		$this->viewParameters = [];
	}

	/**
	 * Home
	 */
	public function home()
	{
		header('Location: /admin/photos');
	}

	/**
	 * Home
	 */
	public function dashboard()
	{
		$cronExecutionRepo = new CronExecutionRepository();
		$photoRepo         = new PhotoRepository();

		$this->viewParameters['cron_executions'] = $cronExecutionRepo->findLatest(7);
		$this->viewParameters['nb_photos']       = $photoRepo->getNbPhotos();

		Views::display('admin/dashboard', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function backupList(Request $request)
	{
		$config                        = new Config();
		$path                          = $config->getParameter('backup_path');
		$dbName                        = $config->getParameter('name');
		$files = File::getList($path);
		$data = [];
		$tmp = preg_split('/_/', $dbName);
		
		if (count($tmp) === 2) {
			$dbNamePos = 3;
			$nbParts   = 4;
		} else {
			$dbNamePos = 2;
			$nbParts   = 3;
		}

		foreach ($files as $file) {
			$tmp = preg_split('/_/', $file);
			
			if (count($tmp) === $nbParts && $tmp[$dbNamePos] === 'agenda.sql') {
				$date = \DateTime::createFromFormat('Ymd_His', $tmp[0] . '_' . $tmp[1]);
				$data[] = [
					'filename' => $file,
					'date'     => $date,
				];
			}
		}

		$this->viewParameters['data'] = $data;
		Views::display('admin/backups/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function backupDownload(Request $request)
	{
		$config   = new Config();
		$date     = $request->get('date');
		$path     = $config->getParameter('backup_path');
		$dbName   = $config->getParameter('name');
		$filePath = $path . $date . '_' . $dbName . '.sql';
		$filename = $date . '_' . $dbName . '.sql';
		
		header('Content-Type: text/sql');
		header('Content-Disposition: attachment; filename=' . $filename);
		readfile($filePath);
		exit();
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function backupDelete(Request $request)
	{
		$config   = new Config();
		$date     = $request->get('date');
		$path     = $config->getParameter('backup_path');
		$dbName   = $config->getParameter('name');
		$filePath = $path . $date . '_' . $dbName . '.sql';
		
		unlink($filePath);
		header('Location: /admin/backups');
		exit();
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function backupCreate(Request $request)
	{
		$controller = new CronController();
		$controller->backup();
		
		header('Location: /admin/backups');
		exit();
	}
}