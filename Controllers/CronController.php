<?php

namespace Controllers;

use Helpers\Views;
use Repositories\CronExecutionRepository;
use Symfony\Component\HttpFoundation\Request;

class CronController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		if ($request->request->has('page')) {
			$page = (int)$request->get('page');
		} else {
			$page = 1;
		}

		$sortBy                        = $request->request->has('sortBy') ? $request->get('sortBy') : 'created_at';
		$sortOrder                     = $request->request->has('sortOrder') ? $request->get('sortOrder') : 'DESC';
		$params                        = [
			$sortBy => $sortOrder,
		];
		$repo                          = new CronExecutionRepository();
		$this->viewParameters['entries'] = $repo->paginate(
			$page,
			CronExecutionRepository::PER_PAGE,
			$params
		);
		$this->viewParameters['current_page']  = $page;
		$this->viewParameters['url']           = '/admin/con-executions';

		Views::display('admin/crons/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function searchForm(Request $request)
	{
		Views::display('admin/crons/search-form', $this->viewParameters);
	}
}
