<?php

namespace Controllers;

use Config\Config;
use Helpers\Auth;
use Helpers\Session;
use Helpers\Tools;
use Helpers\Views;
use Repositories\CommentRepository;
use Repositories\PhotoRepository;
use Repositories\TagRepository;
use Repositories\TranslationRepository;
use Repositories\YearRepository;
use Services\HCaptcha;
use Services\Photo;
use Services\Translation;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct()
	{
		parent::__construct();

		if (isset($_COOKIE['apod_vidry_ca']) && strlen($_COOKIE['apod_vidry_ca']['lang']) === 2) {
			Session::set('lang', $_COOKIE['apod_vidry_ca']['lang']);
		} elseif (!Session::has('lang') || !is_string(Session::get('lang'))) {
			Session::set('lang', 'en');
		} elseif (Session::get('lang') === 'fr') {
			setlocale(LC_TIME, "fr_FR.utf8");
		} else {
			Session::set('lang', 'en');
		}

		$this->settings       = Session::get('settings');
		$translations         = Translation::findByLang(Session::get('lang'));
		$this->viewParameters['settings']     = $this->settings;
		$this->viewParameters['lang']         = Session::get('lang');
		$this->viewParameters['translations'] = $translations;
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function index(Request $request)
	{
		$tagRepo     = new TagRepository();
		$commentRepo = new CommentRepository();
		$photo       = null;
		$params      = [];
		$config      = new Config();
		$cdnUrl      = $config->getParameter('cdn_url');
		$baseUrl     = $config->getParameter('base_url');

		if ($request->request->has('lang') && $request->get('lang') !== '') {
			$lang = $request->get('lang');

			if (in_array($lang, ['en', 'fr'])) {
				Session::set('lang', $lang);
				$this->viewParameters['lang']         = $lang;
				$this->viewParameters['translations'] = Translation::findByLang($lang);
			}
		}

		if ($request->request->has('date') && $request->get('date') !== '') {
			if ($request->get('date') === 'random') {
				$photo = Photo::getRandom();
				$date  = new \DateTime($photo->getProperty('date'));
			} else {
				$date = \DateTime::createFromFormat('ymd', $request->get('date'));

				if ($date !== false) {
					$photo = Photo::getPhoto($date);
				}
			}
		} else {
			$date  = new \DateTime();
			$photo = Photo::getTodayPhoto();
		}

		if ($photo === null) {
			$date  = null;
			$photo = Photo::getLatest();
		}

		if ($date === null) {
			$parts    = preg_split('/\./', $photo->getProperty('filename'));
			$filename = $parts[0];
			$date     = \DateTime::createFromFormat('ymd', $filename);
		}

		// Checks if the image exists on CDN
		$this->viewParameters['has_cdn']  = Photo::checkCdn($photo->getProperty('filename'));
		$this->viewParameters['cdn_url']  = $cdnUrl;
		$this->viewParameters['base_url'] = $baseUrl;

		$dateStr                              = $date->format('ymd');
		$this->viewParameters['previous_day'] = Photo::getPreviousDay($dateStr);
		$this->viewParameters['next_day']     = Photo::getNextDay($dateStr);

		if ($photo !== null) {
			$params['photo_id']               = $photo->getId();
			$this->viewParameters['tags']     = $tagRepo->findByPhoto($photo->getId(), true);
			$this->viewParameters['comments'] = $commentRepo->fetchAll($params);
		}

		if (Session::get('lang') === 'fr') {
			$parser  = new \IntlDateFormatter(
				'fr_FR',
				\IntlDateFormatter::FULL,
				\IntlDateFormatter::FULL,
				'America/New_York',
				\IntlDateFormatter::GREGORIAN,
				'dd LLLL yyyy'
			);
			$dateStr = $parser->format($date);
		} else {
			$dateStr = date("F j, Y", $date->getTimestamp());
		}

		$this->viewParameters['page']     = 'index';
		$this->viewParameters['date']     = $date;
		$this->viewParameters['date_str'] = $dateStr;

		if ($photo !== null) {
			$video = $photo->getProperty('video');

			if ($video !== '') {
				$this->viewParameters['video'] = $video;
			} else {
				$parts = preg_split('/\./', $photo->getProperty('filename'));

				$this->viewParameters['filename']  = $parts[0];
				$this->viewParameters['extension'] = $parts[1];
			}

			$this->viewParameters['photo'] = $photo;
		} else {
			$this->viewParameters['not_found'] = true;
		}

		Views::display('photo', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function calendar(Request $request)
	{
		$year      = null;
		$month     = null;
		$photos    = [];
		$months    = [];
		$years     = [];
		$photoRepo = new PhotoRepository();
		$yearRepo  = new YearRepository();

		if ($request->request->has('year') && $request->get('year') !== '') {
			$year = (int)$request->get('year');
		}

		if ($request->request->has('month') && $request->get('month') !== '') {
			$month = (int)$request->get('month');
		}

		if ($month !== null && $year !== null) {
			$photos = $photoRepo->findByMonth($year, $month);
		} elseif ($year !== null) {
			for ($i = 1; $i <= 12; $i++) {
				$date = new \DateTime();
				$date->setDate(1970, $i, 1);

				if (Session::get('lang') === 'fr') {
					$parser     = new \IntlDateFormatter(
						'fr_FR',
						\IntlDateFormatter::FULL,
						\IntlDateFormatter::FULL,
						'America/New_York',
						\IntlDateFormatter::GREGORIAN,
						'LLLL'
					);
					$months[$i] = ucfirst($parser->format($date));
				} else {
					$months[$i] = $date->format('F');
				}
			}
		} else {
			$years = $yearRepo->findActive();
		}

		$this->viewParameters['page'] = 'calendar';

		if (!empty($years)) {
			$this->viewParameters['years'] = $years;

			Views::display('calendar', $this->viewParameters);
		} elseif (!empty($months)) {
			$this->viewParameters['months']        = $months;
			$this->viewParameters['year']          = $year;
			$this->viewParameters['previous_year'] = $year - 1;
			$this->viewParameters['next_year']     = $year + 1;
			$this->viewParameters['current_year']  = date('Y');
			$this->viewParameters['current_month'] = date('n');

			Views::display('calendar', $this->viewParameters);
		} else {
			$date = new \DateTime();
			$date->setDate($year, $month, 1);
			$firstDayOfWeek = $date->format('w');
			$nextMonth      = $month + 1;
			$previousMonth  = $month - 1;
			$nextYear       = $year;
			$previousYear   = $year;

			if ($nextMonth === 13) {
				$nextMonth = 1;
				$nextYear  = $year + 1;
			}

			if ($previousMonth === 0) {
				$previousMonth = 12;
				$previousYear  = $year - 1;
			}

			$previousDate = new \DateTime();
			$previousDate->setDate($previousYear, $previousMonth, 1);
			$nextDate = new \DateTime();
			$nextDate->setDate($nextYear, $nextMonth, 1);
			$daysInMonth = cal_days_in_month(
				CAL_GREGORIAN,
				$date->format('m'),
				$date->format('Y')
			);

			$this->viewParameters['photos']            = $photos;
			$this->viewParameters['limits']            = $this->findLimits($photos);
			$this->viewParameters['first_day_of_week'] = $firstDayOfWeek;
			$this->viewParameters['year']              = $year;
			$this->viewParameters['month']             = $date->format('m');
			$this->viewParameters['days_in_month']     = $daysInMonth;
			$this->viewParameters['previous_month']    = $previousMonth;
			$this->viewParameters['previous_year']     = $previousYear;
			$this->viewParameters['previous_date']     = $previousDate;
			$this->viewParameters['next_month']        = $nextMonth;
			$this->viewParameters['next_year']         = $nextYear;
			$this->viewParameters['next_date']         = $nextDate;

			if (Session::get('lang') === 'fr') {
				$parser = new \IntlDateFormatter(
					'fr_FR',
					\IntlDateFormatter::FULL,
					\IntlDateFormatter::FULL,
					'America/New_York',
					\IntlDateFormatter::GREGORIAN,
					'LLLL'
				);
			} else {
				$parser = new \IntlDateFormatter(
					'en_US',
					\IntlDateFormatter::FULL,
					\IntlDateFormatter::FULL,
					'America/New_York',
					\IntlDateFormatter::GREGORIAN,
					'LLLL'
				);
			}

			$this->viewParameters['month_str'] = ucfirst($parser->format($date));

			Views::display('gallery', $this->viewParameters);
		}
	}

	public function mosaic(Request $request)
	{
		if ($request->request->has('year')) {
			$year = (int)$request->get('year');
		} else {
			$year = date('Y');
		}

		$repo   = new PhotoRepository();
		$photos = $repo->findAllByYear($year);

		$this->viewParameters['photos'] = $photos;
		$this->viewParameters['year']   = $year;
		$this->viewParameters['page']   = 'mosaic';

		Views::display('mosaic', $this->viewParameters);
	}

	public function scroll(Request $request)
	{
		$repo    = new PhotoRepository();
		$photos  = $repo->findPreviousTen();
		$config  = new Config();
		$baseUrl = $config->getParameter('base_url');

		$this->viewParameters['photos']   = $photos;
		$this->viewParameters['page']     = 'scroll';
		$this->viewParameters['base_url'] = $baseUrl;

		Views::display('scroll', $this->viewParameters);
	}

	public function getPreviousForScroll(Request $request)
	{
		$repo   = new PhotoRepository();
		$photos = $repo->findPreviousTen((int)$request->get('latest_id'), 5);

		echo json_encode($photos);
		exit();
	}

	public function searchForm()
	{
		if (Session::has('lang')) {
			$lang = Session::get('lang');
		} else {
			$lang = 'en';
		}

		$yearRepo                     = new YearRepository();
		$months                       = [];
		$years                        = $yearRepo->findActive();
		$tagRepo                      = new TagRepository();
		$params                       = [
			'sortBy'    => 'tag_' . $lang,
			'sortOrder' => 'ASC',
		];
		$this->viewParameters['tags'] = $tagRepo->fetchAll($params);

		for ($i = 1; $i <= 12; $i++) {
			$date = new \DateTime();
			$date->setDate(1970, $i, 1);
			$months[$i] = date('F', $date->format('U'));
		}

		Session::set('search_params', []);

		$this->viewParameters['page']   = 'search';
		$this->viewParameters['months'] = $months;
		$this->viewParameters['years']  = $years;

		Views::display('search-form', $this->viewParameters);
	}

	public function search(Request $request)
	{
		$sortBy    = $request->request->has('sort_by') ? $request->get('sort_by') : 'date';
		$sortOrder = $request->request->has('sort_order') ? $request->get('sort_order') : 'DESC';

		if (Session::has('search_params')) {
			$searchParams = Session::get('search_params');
		} else {
			$searchParams = [];
		}

		if (!isset($searchParams['sort_by'])
			|| $searchParams['sort_by'] !== $sortBy
			|| $searchParams['sort_order'] !== $sortOrder
		) {
			$searchParams['sort_by']    = $sortBy;
			$searchParams['sort_order'] = $sortOrder;
		}

		if ($request->request->has('year') && $request->get('year') !== '') {
			$searchParams['year'] = $request->get('year');
		}

		if ($request->request->has('month') && $request->get('month') !== '') {
			$searchParams['month'] = $request->get('month');
		}

		if ($request->request->has('username') && $request->get('username') !== '') {
			$searchParams['username'] = $request->get('username');
		}

		if ($request->request->has('tag') && $request->get('tag') !== '') {
			$tag                 = $request->get('tag');
			$searchParams['tag'] = $tag;

			if (Session::has('lang')) {
				$lang = Session::get('lang');
			} else {
				$lang = 'en';
			}

			$tagRepo                       = new TagRepository();
			$params                        = [
				'sortBy'    => 'tag_' . $lang,
				'sortOrder' => 'ASC',
			];
			$this->viewParameters['tags']  = $tagRepo->fetchAll($params);
			$searchParams['selected_tags'] = preg_split('/,/', $tag);
		}

		if ($request->request->has('keyword') && $request->get('keyword') !== '') {
			$searchParams['keyword'] = $request->get('keyword');
		}

		if ($request->request->has('page')) {
			$page = (int)$request->get('page');
		} else {
			$page = 1;
		}

		Session::set('search_params', $searchParams);
		$paging = Photo::search($searchParams, $page);

		$this->viewParameters['page']          = 'search';
		$this->viewParameters['paging']        = $paging;
		$this->viewParameters['current_page']  = $page;
		$this->viewParameters['sort_by']       = $sortBy;
		$this->viewParameters['sort_order']    = $sortOrder;
		$this->viewParameters['search_params'] = $searchParams;
		$this->viewParameters['url']           = '/search';

		if (Auth::isAdmin()) {
			$this->viewParameters['is_admin'] = true;
		}

		Views::display('search-result', $this->viewParameters);
	}

	public function searchByTag(Request $request)
	{
		Session::set('search_params', []);
		$this->search($request);
	}

	public function searchByUsername(Request $request)
	{
		Session::set('search_params', []);
		$this->search($request);
	}

	public function slideshow(Request $request)
	{
		$repo  = new TranslationRepository();
		$title = $repo->findByLangAndCode(Session::get('lang'), 'slideshow');
		$title = $title['value'];

		$repo = new PhotoRepository();

		if ($request->request->has('year') && $request->get('year') !== '') {
			$year = $request->get('year');

			if ($year === 'random') {
				$photos                         = [
					Photo::getRandom(),
				];
				$this->viewParameters['random'] = true;
			} elseif ($request->request->has('month') && $request->get('month') !== '') {
				$month  = $request->get('month');
				$photos = $repo->findAllByMonth((int)$year, $month, true);

				$this->viewParameters['month'] = $month;
			} else {
				$photos = $repo->findAllByYear((int)$year, true);
			}

			$this->viewParameters['year'] = $year;
		} else {
			$year   = (int)date('Y');
			$photos = $repo->findAllByYear($year, true);
		}

		$title .= ' - ' . $year;

		if (count($photos) > 0) {
			$this->viewParameters['title'] = $title;
			$this->viewParameters['photo'] = $photos[0];
			$this->viewParameters['lang']  = Session::get('lang');
		}

		Views::display('slideshow', $this->viewParameters);
	}

	public function getSlide(Request $request)
	{
		if ($request->request->has('current')) {
			$lang = Session::get('lang');
			$data = Photo::findForSlideShow($request);

			if ($lang === 'fr') {
				$title = $data['date']->format('d F Y') . ' - ';
			} else {
				$title = $data['date']->format('F d, Y') . ' - ';
			}

			if ($data['photo']->getProperty('title_' . $lang) !== '') {
				$title .= $data['photo']->getProperty('title_' . $lang);
			} else {
				$title .= $data['photo']->getProperty('title_en');
			}

			$result = [
				'success'     => 'true',
				'filename'    => $data['photo']->getProperty('filename'),
				'title'       => $title,
				'description' => $data['photo']->getProperty('description_' . $lang),
				'date'        => $data['date']->format('ymd'),
			];
		} else {
			$result = [
				'success' => 'false',
			];
		}

		echo json_encode($result);
		exit();
	}

	public function about()
	{
		Views::display('about', $this->viewParameters);
	}

	public function contact(Request $request)
	{
		$config   = new Config();
		$hCaptcha = $config->getParameter('h_captcha');

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		if ($request->request->has('success')) {
			$this->viewParameters['success'] = $request->get('success');
		}

		$this->viewParameters['h_captcha'] = $hCaptcha;

		Views::display('contact', $this->viewParameters);
	}

	public function sendMessage(Request $request)
	{
		$hCaptchaResponse = HCaptcha::check($request);

		if ($hCaptchaResponse) {
			$errors  = [];
			$success = false;
			$name    = '';
			$email   = '';
			$subject = '';
			$message = '';

			if (!$request->request->has('email') || $request->get('email') === '') {
				$errors[] = 'email';
			} else {
				$email = $request->get('email');
			}

			if (!$request->request->has('subject') || $request->get('subject') === '') {
				$errors[] = 'subject';
			} else {
				$subject = $request->get('subject');
			}

			if (!$request->request->has('message') || $request->get('message') === '') {
				$errors[] = 'message';
			} else {
				$message = $request->get('message');
			}

			if ($request->request->has('name') && $request->get('name') !== '') {
				$name = $request->get('name');
			}

			if (count($errors) === 0) {
				$subject = 'Message from apod.vidry.ca: ' . $subject;
				$result  = Tools::sendEmail($email, $name, $subject, $message);

				if ($result !== false) {
					$request->request->set('success', true);
				}
			}
		} else {
			$errors[] = 'captcha';
		}

		if (count($errors)) {
			$request->request->set('errors', $errors);
		}

		$this->contact($request);
	}

	public function switchLang(Request $request)
	{
		if ($request->request->has('lang') && in_array($request->get('lang'), ['en', 'fr'])) {
			Session::set('lang', $request->get('lang'));
		}

		if ($request->request->has('date') && $request->get('date') !== '') {
			$location = '/day/' . $request->get('date');
		} else {
			$location = '/';
		}

		header('Location: ' . $location);
	}

	public function switchFr(Request $request)
	{
		Session::set('lang', 'fr');

		$this->__construct();
		$this->index($request);
		exit();
	}

	public function login()
	{
		Views::display('login', $this->viewParameters);
	}

	public function permissionDenied()
	{
		Views::display('permission-denied', $this->viewParameters);
	}

	public function processLogin(Request $request)
	{
		if ($request->request->has('username') && $request->get('username') !== null
			&& $request->request->has('password') && $request->get('password')
		) {
			if ($request->request->has('remember_me')) {
				$rememberMe = true;
			} else {
				$rememberMe = false;
			}

			$result = Auth::login(
				$request->get('username'),
				$request->get('password'),
				$rememberMe
			);

			if ($result) {
				header('Location: /admin/photos');
			} else {
				header('Location: /login/error');
			}
			exit();
		}
	}

	public function logout()
	{
		Auth::logout();
		header('Location: /');
	}

	private function findLimits(array $photos)
	{
		$first = null;
		$last = null;
		$lastIndex = count($photos) - 1;

		if (isset($photos[0]) && $photos[0]->getProperty('filename') !== '') {
			$first = (int)substr($photos[0]->getProperty('filename'), 4, 2);
		}

		if (isset($photos[$lastIndex]) && $photos[$lastIndex]->getProperty('filename') !== '') {
			$last = (int)substr($photos[$lastIndex]->getProperty('filename'), 4, 2);
		}

		return [
			'first' => $first,
			'last'  => $last,
		];
	}
}