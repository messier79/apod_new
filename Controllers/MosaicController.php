<?php

namespace Controllers;

use Services\Photo;
use Symfony\Component\HttpFoundation\Request;

class MosaicController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);

		$this->viewParameters = [];
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		if ($request->request->has('error')) {
			$this->viewParameters['error'] = $request->get('error');
		}

		if ($request->request->has('success')) {
			$this->viewParameters['success'] = $request->get('success');
		}

		echo $this->twig->render('admin/mosaics/list.html.twig', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @return void
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function generate(Request $request)
	{
		if ($request->request->has('year')) {
			$year = $request->get('year');
			$result = Photo::generateYearMosaic($year);

			if ($result === false) {
				$request->request->set('error', 'No file for ' . $year);
			} else {
				$request->request->set('success', true);
			}
		} else {
			$request->request->set('error', 'The year is missing');
		}

		$this->list($request);
	}
}