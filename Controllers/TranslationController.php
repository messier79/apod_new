<?php

namespace Controllers;

use Helpers\Views;
use Models\Translation;
use Repositories\TranslationRepository;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class TranslationController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		$sortBy                        = $request->request->has('sortBy') ? $request->get('sortBy') : 'code, lang';
		$sortOrder                     = $request->request->has('sortOrder') ? $request->get('sortOrder') : 'ASC';
		$params                        = [
			$sortBy => $sortOrder,
		];
		$repo                          = new TranslationRepository();
		$this->viewParameters['translations'] = $repo->fetchAll($params);

		Views::display('admin/translations/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function form(Request $request)
	{
		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$translation = new Translation((int)$request->get('id'));

			if ((int)$request->get('id') === 0) {
				$translation->setProperty('id', $request->get('id'))
					->setProperty('lang', $request->get('lang'))
					->setProperty('code', $request->get('code'))
					->setProperty('value', $request->get('value'));
			}
		} else {
			$translation = new Translation();
			$translation->setProperty('id', 0)
				->setProperty('lang', '')
				->setProperty('code', '')
				->setProperty('value', '');
		}
		$this->viewParameters['translation'] = $translation;

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		Views::display('admin/translations/form', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(Translation::class, $request->request->all());

		if (count($errors) === 0) {
			$id   = (int)$request->get('id');
			$date = new \DateTime();
			$translation = new Translation($id);
			$translation->setProperty('lang', $request->get('lang'))
						->setProperty('code', $request->get('code'))
						->setProperty('value', $request->get('value'))
				->setProperty('updated_at', $date->format('Y-m-d H:i'));

			if ($id === 0) {
				$translation->setProperty('created_at', $date->format('Y-m-d H:i'));
			}

			if (count($errors) === 0) {
				$translation->save();
			}
		}

		if (count($errors) === 0) {
			$request->request->set('success', ['saved']);
			$this->list($request);
		} else {
			$request->request->set('errors', $errors);
			$this->form($request);
		}
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function delete(Request $request)
	{
		$translation = new Translation((int)$request->get('id'));
		$translation->setProperty('deleted_at', new \DateTime())
			->save();

		$this->list($request);
	}
}
