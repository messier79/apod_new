<?php

namespace Controllers;

use Services\Photo;
use Symfony\Component\HttpFoundation\Request;

class ThumbnailController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);

		$this->viewParameters = [];
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		if ($request->request->has('year')) {
			$year  = $request->get('year');
		} else {
			$year  = date('Y');
		}

		$date = new \DateTime();
		$date->setDate($year, 1, 1);
		$previousYear  = $year - 1;
		$nextYear      = $year + 1;

		$this->viewParameters['date']           = $date;
		$this->viewParameters['next_year']      = $nextYear;
		$this->viewParameters['previous_year']  = $previousYear;
		$this->viewParameters['pending'] = Photo::listPendingThumbnails();

		if ($request->request->has('error')) {
			$this->viewParameters['error'] = $request->get('error');
		}

		if ($request->request->has('success')) {
			$this->viewParameters['success'] = $request->get('success');
		}

		echo $this->twig->render('admin/thumbnails/list.html.twig', $this->viewParameters);
	}

	public function upload(Request $request)
	{
		if (is_uploaded_file($_FILES['thumbnail']['tmp_name'])) {
			$year       = $request->get('year');
			$month      = $request->get('month');
			$sourcePath = $_FILES['thumbnail']['tmp_name'];
			$targetPath = __DIR__ . '/../storage/' . $year . $month . '.png';

			if (move_uploaded_file($sourcePath, $targetPath)) {
				$this->list($request);
			}
		}
	}

	/**
	 * @param Request $request
	 * @return void
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function generate(Request $request)
	{
		if ($request->request->has('year')
			&& $request->request->has('month')
			&& $request->request->has('nb_lines')
		) {
			$year = $request->get('year');
			$month = $request->get('month');
			$nbLines = $request->get('nb_lines');
			$result = Photo::generateMonthThumbnail(
				$year,
				$month,
				$nbLines
			);

			if ($result === false) {
				$request->request->set('error', 'No file for ' . $year . '-' . $month);
			} else {
				$request->request->set('success', true);
			}
		} else {
			$request->request->set('error', 'A parameter is missing ({year}/{month}/{nb_lines})');
		}

		$this->list($request);
	}
}