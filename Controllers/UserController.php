<?php

namespace Controllers;

use Models\User;
use Repositories\UserRepository;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AuthenticatedController
{
	const ADMIN_GROUP_ID = 1;
	const USER_GROUP_ID  = 2;

	public function __construct()
	{
		parent::__construct(true);
		
		$this->viewParameters = [];
	}

	/**
	 * List
	 *
	 * @param Request $request
	 */
	public function list(Request $request)
	{
		$sortBy = $request->request->has('sortBy') ? $request->get('sortBy') : 'username';
		$sortOrder = $request->request->has('sortOrder') ? $request->get('sortOrder') : 'ASC';
		$params = [
			'sortBy' => $sortBy,
			'sortOrder' => $sortOrder,
		];
		$repo = new UserRepository();
		$this->viewParameters['users'] = $repo->fetchAll($params);

		echo $this->twig->render('admin/users/list.html.twig', $this->viewParameters);
	}

	/**
	 * Form
	 *
	 * @var Request $request
	 */
	public function form(Request $request)
	{
		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$user = new User((int)$request->get('id'));

			if ((int)$request->get('id') === 0) {
				$user->setProperty('id', $request->get('id'))
					->setProperty('group_id', $request->get('group_id'))
					->setProperty('username', $request->get('username'))
					->setProperty('email', $request->get('email'));
			}
		} else {
			$user = new User();
			$user->setProperty('id', 0)
				->setProperty('group_id', 0)
				->setProperty('username', '')
				->setProperty('email', '');
		}
		$this->viewParameters['user'] = $user;

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		echo $this->twig->render('admin/users/form.html.twig', $this->viewParameters);
	}

	/**
	 * Save
	 *
	 * @var Request $request
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(User::class, $request->request->all());

		if (count($errors) === 0) {
			$id = (int)$request->get('id');
			$user = new User($id);
			$user->setProperty('group_id', $request->get('group_id'))
				->setProperty('username', $request->get('username'))
				->setProperty('email', $request->get('email'));

			if ($request->get('password') !== '') {
				if ($request->get('password') === $request->get('password2')) {
					$password = password_hash($request->get('password'), PASSWORD_DEFAULT);
					$user->setProperty('password', $password);
				} else {
					$errors['password'] = 'mismatch';
				}
			} elseif ($id === 0) {
				$errors['password'] = 'mandatory';
			}

			if (count($errors) === 0) {
				$user->save();
			}
		}

		if (count($errors) === 0) {
			$request->request->set('success', ['saved']);
			$this->list($request);
		} else {
			$request->request->set('errors', $errors);
			$this->form($request);
		}
	}

	/**
	 * Delete
	 *
	 * @var Request $request
	 */
	public function delete(Request $request)
	{
		$user = new User((int)$request->get('id'));
		$user->setProperty('deleted_at', new \DateTime())
			->save();

		$this->list($request);
	}
}