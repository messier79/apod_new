<?php

namespace Controllers;

use Config\Config;
use Helpers\Tools;
use Helpers\Views;
use Models\Comment as CommentEntity;
use Models\Photo;
use Repositories\PhotoRepository;
use Repositories\CommentRepository;
use Services\Comment;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		$this->viewParameters = $this->getList($request);

		Views::display('comments/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function admin(Request $request)
	{
		$this->viewParameters = $this->getList($request);

		Views::display('admin/comments/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function form(Request $request)
	{
		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$comment = new CommentEntity((int)$request->get('id'));
		} else {
			$comment = new CommentEntity();
		}
		$this->viewParameters['comment'] = $comment;

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		Views::display('admin/comments/form', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(CommentEntity::class, $request->request->all());

		if (count($errors) === 0) {
			$id   = (int)$request->get('id');
			$date = new \DateTime();
			$comment = new CommentEntity($id);
			$photoId = (int)$request->get('photo_id');
			$username = $request->get('username');
			$content = $request->get('comment');
			$photo = new Photo($photoId);
			$comment->setProperty('photo_id', $photoId)
				->setProperty('username', $username)
				->setProperty('comment', $content)
				->setProperty('updated_at', $date->format('Y-m-d H:i'));

			if ($id === 0) {
				$comment->setProperty('created_at', $date->format('Y-m-d H:i'));
			}

			if (count($errors) === 0) {
				$comment->save();
			}
		}

		if (!$request->request->has('fromSite')) {
			if (count($errors) > 0) {
				$request->request->set('success', ['saved']);
				$this->list($request);
			} else {
				$request->request->set('errors', $errors);
				$this->form($request);
			}
		} else {
			$config = new Config();
			$email = $config->getParameter('contact_recipient');
			$name = 'APOD';
			$subject = 'APOD - New Comment';
			$parameters = [
				'photo' => $photo,
				'username' => $username,
				'comment' => $comment,
			];
			$message = Views::display('emails/new-comment', $parameters, false);

			Tools::sendEmail($email, $name, $subject, $message);
		}
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function delete(Request $request)
	{
		$comment = new CommentEntity((int)$request->get('id'));
		$comment->setProperty('deleted_at', new \DateTime())
			->save();

		$request->request->set('photo_id', $comment->getProperty('photo_id'));

		$this->admin($request);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	private function getList(Request $request)
	{
		$photoId                       = $request->request->has('photo_id') ? $request->get('photo_id') : 0;
		$sortBy                        = $request->request->has('sortBy') ? $request->get('sortBy') : 'created_at';
		$sortOrder                     = $request->request->has('sortOrder') ? $request->get('sortOrder') : 'DESC';
		$params                        = [
			'photo_id'  => $photoId,
			'sortBy'    => $sortBy,
			'sortOrder' => $sortOrder,
		];
		$repo                          = new CommentRepository();
		$this->viewParameters['comments'] = $repo->fetchAll($params);

		return $this->viewParameters;
	}
}
