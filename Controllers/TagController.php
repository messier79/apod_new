<?php

namespace Controllers;

use Helpers\Views;
use Models\Tag as TagEntity;
use Repositories\PhotoRepository;
use Repositories\TagRepository;
use Services\Tag;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class TagController extends AuthenticatedController
{
	public function __construct()
	{
		parent::__construct(true);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		$sortBy                        = $request->request->has('sortBy') ? $request->get('sortBy') : 'tag_en';
		$sortOrder                     = $request->request->has('sortOrder') ? $request->get('sortOrder') : 'ASC';
		$params                        = [
			'sortBy'    => $sortBy,
			'sortOrder' => $sortOrder,
		];
		$repo                          = new TagRepository();
		$this->viewParameters['tags'] = $repo->fetchAll($params);
		$this->viewParameters['photos_by_tag'] = $repo->findPhotosByTag();

		Views::display('admin/tags/list', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function form(Request $request)
	{
		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$tag = new TagEntity((int)$request->get('id'));

			if ((int)$request->get('id') === 0) {
				$tag->setProperty('id', $request->get('id'))
					->setProperty('tag_en', $request->get('tag_en'))
					->setProperty('tag_fr', $request->get('tag_fr'));
			}
		} else {
			$tag = new TagEntity();
			$tag->setProperty('id', 0)
				->setProperty('tag_en', '')
				->setProperty('tag_fr', '');
		}
		$this->viewParameters['tag'] = $tag;

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		Views::display('admin/tags/form', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(TagEntity::class, $request->request->all());

		if (count($errors) === 0) {
			$id   = (int)$request->get('id');
			$date = new \DateTime();
			$tag = new TagEntity($id);
			$tag->setProperty('tag_en', $request->get('tag_en'))
				->setProperty('tag_fr', $request->get('tag_fr'))
				->setProperty('updated_at', $date->format('Y-m-d H:i'));

			if ($id === 0) {
				$tag->setProperty('created_at', $date->format('Y-m-d H:i'));
			}

			if (count($errors) === 0) {
				$tag->save();
			}
		}

		if (count($errors) === 0) {
			$request->request->set('success', ['saved']);
			$this->list($request);
		} else {
			$request->request->set('errors', $errors);
			$this->form($request);
		}
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function delete(Request $request)
	{
		$tag = new TagEntity((int)$request->get('id'));
		$tag->setProperty('deleted_at', new \DateTime())
			->save();

		$this->list($request);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function populate(Request $request)
	{
		if ($request->request->has('id') && (int)$request->get('id') > 0) {
			$reset = false;

			if ($request->request->has('reset')) {
				$reset = (bool)$request->get('reset');
			}

			$tag = new TagEntity((int)$request->get('id'));
			Tag::populate($tag, $reset);
		}

		$this->list($request);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function populateCountry(Request $request)
	{
		$tagRepo = new TagRepository();
		$photoRepo = new PhotoRepository();
		$countries = $tagRepo->findCountries($request->get('letter'));

		foreach ($countries as $country) {
			$photos = $photoRepo->findByStringInDescription($country['country_name']);

			if (count($photos) > 0) {
				echo $country['country_name'] . ' => ' . count($photos) . '<br>';
			}
		}

		die();
		if ($request->request->has('id') && (int)$request->get('id') > 0) {
			$reset = false;

			if ($request->request->has('reset')) {
				$reset = (bool)$request->get('reset');
			}

			$tag = new TagEntity((int)$request->get('id'));
			Tag::populate($tag, $reset);
		}

		$this->list($request);
	}
}
