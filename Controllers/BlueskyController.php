<?php

namespace Controllers;

use Config\Config;
use Helpers\Tools;
use Services\Bluesky;
use Services\Photo;
use Symfony\Component\HttpFoundation\Request;

class BlueskyController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct(bool $fromCron = false)
	{
		parent::__construct();
	}

	/**
	 * @param Request $request
	 */
	public function dailyPost(Request $request)
	{
		if ($request->request->has('lang')) {
			$lang = $request->get('lang');
		} else {
			$lang = 'en';
		}

		$filename = '';
		$date = new \DateTime();

		if ($date !== false) {
			$photo = Photo::getPhoto($date);
			$filename = $photo->getProperty('filename');
		}

		$config  = new Config();
		$baseUrl = $config->getParameter('base_url');
		$url     = $baseUrl . 'day/' . date('ymd') . '/' . $lang;

		if ($lang === 'fr') {
			$content = '#imageastrodujour #apod ';
		} else {
			$content = '#astronomypictureoftheday #apod ';
		}

		$content .= $photo->getProperty('title_' . $lang);

		Tools::log('Daily Bluesky cron (apod.vidry.ca) - ' . $lang);

		Bluesky::post($content, $filename, $url);
	}
}
