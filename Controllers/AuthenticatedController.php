<?php

namespace Controllers;

use Helpers\Auth;

class AuthenticatedController extends DefaultController
{
	public function __construct($admin = false)
	{
		parent::__construct();

		if (!Auth::isAuthenticated()) {
			header('Location: /login');
		} elseif ($admin === true && !Auth::isAdmin()) {
			header('Location: /permission-denied');
		}
	}
}