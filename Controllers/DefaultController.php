<?php
namespace Controllers;

use Helpers\Auth;
use Helpers\Middleware;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class DefaultController
{
	/**
	 * @var array
	 */
	protected $viewParameters;

    public function __construct()
    {
        $middleware = new Middleware();

		if (Auth::isAdmin()) {
			$this->viewParameters['is_admin'] = true;
		}

		$loader = new FilesystemLoader(__DIR__ . '/../Resources/views');
		$this->twig = new Environment($loader);
    }
}