<?php

namespace Controllers;

use Config\Config;
use Helpers\Auth;
use Helpers\Db;
use Helpers\Image;
use Helpers\Session;
use Helpers\Tools;
use Helpers\Views;
use Repositories\PhotoRepository;
use Services\Nasa;
use Services\Photo;
use Services\Translation;
use Services\Translator;
use Symfony\Component\HttpFoundation\Request;
use voku\cache\Cache;

class ToolsController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct(bool $fromCron = false)
	{
		parent::__construct();

		if (!Session::has('lang')) {
			Session::set('lang', 'en');
		} elseif (Session::get('lang') === 'fr') {
			setlocale(LC_TIME, "fr_FR");
		}

		$this->settings       = Session::get('settings');
		$translations         = Translation::findByLang(Session::get('lang'));
		$this->viewParameters = [
			'settings'     => $this->settings,
			'lang'         => Session::get('lang'),
			'translations' => $translations,
		];
	}

	public function getPhoto(Request $request)
	{
		if (!$request->request->has('filename')) {
			echo 'No Album ID';
			exit;
		}

		$config    = new Config();
		$thumbDir  = '';
		$photosDir = $config->getParameter('photos_directory');
		$filename  = $request->get('filename');
		$extension = $request->get('extension');
		$year      = substr($filename, 0, 2);

		if ($request->request->has('thumb') && $request->get('thumb') === 'thumb') {
			$thumbDir .= '_sfpg_data/thumb/';
		}

		if ($year >= 90) {
			$file = $photosDir . '19' . $year;
		} else {
			$file = $photosDir . '20' . $year;
		}

		$file .= '/' . $thumbDir . $filename . '.' . $extension;

		if (!file_exists($file)) {
			$file      = __DIR__ . '/../Resources/assets/images/default.png';
			$extension = 'png';
		}

		switch ($extension) {
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
			case 'gif':
				header('Content-Type: image/gif');
				break;
		}

		header('Content-Length: ' . filesize($file));
		echo file_get_contents($file);

		exit;
	}

	public function getTodayPhoto(Request $request)
	{
		$repo  = new PhotoRepository();
		$photo = $repo->findLatest();
		$tmp   = preg_split('/\./', $photo->getProperty('filename'));

		$request->request->set('filename', $tmp[0]);
		$request->request->set('extension', $tmp[1]);

		$this->getPhoto($request);
		exit();
	}

	public function exportForm(Request $request)
	{
		$months = [];

		for ($i = 1; $i <= 12; $i++) {
			$date = new \DateTime();
			$date->setDate(1970, $i, 1);

			$parser     = new \IntlDateFormatter(
				'fr_FR',
				\IntlDateFormatter::FULL,
				\IntlDateFormatter::FULL,
				'America/New_York',
				\IntlDateFormatter::GREGORIAN,
				'LLLL'
			);
			$months[$i] = ucfirst($parser->format($date));
		}

		$this->viewParameters['year']   = date('Y');
		$this->viewParameters['months'] = $months;

		Views::display('tools/export-form', $this->viewParameters);
	}

	public function export(Request $request)
	{
		$data     = [];
		$year     = null;
		$month    = null;
		$day      = null;
		$filename = 'apod';

		if ($request->request->has('year') && $request->get('year') !== '') {
			$year     = (int)$request->get('year');
			$filename .= '_' . $year;
		} else {
			$data['error'] = 'You must specify a year to export. Format /export/year/(month)/(day)';
		}

		if ($request->request->has('month') && $request->get('month') !== 'all') {
			$month    = (int)$request->get('month');
			$filename .= '_' . $month;
		}

		if ($request->request->has('day') && $request->get('day') !== '') {
			$day      = (int)$request->get('day');
			$filename .= '_' . $day;
		}

		$data = Tools::export($year, $month, $day);

		if ($request->request->has('download') && $request->get('download') === '1') {
			header('Content-disposition: attachment; filename=' . $filename . '.json');
		}

		header('Content-type: application/json');

		echo json_encode($data);
		exit;
	}

	public function getTodayJson(Request $request)
	{
		$config  = new Config();
		$baseUrl = $config->getParameter('base_url');
		$repo    = new PhotoRepository();
		$photo   = $repo->findLatest();
		$data    = [
			'photo' => Tools::objectToArray($photo),
			'image' => $baseUrl . 'photo/today',
			'thumb' => $baseUrl . 'photo/today/thumb',
		];

		header('Content-type: application/json');

		echo str_replace('\u0000*\u0000', '', json_encode($data, JSON_UNESCAPED_UNICODE));
		exit;
	}

	public function populate(Request $request)
	{
		Views::display('tools/populate', []);
	}

	public function populateDates(Request $request)
	{
		if ($request->request->has('limit')) {
			$limit = (int)$request->get('limit');
		} else {
			$limit = 500;
		}

		$repo = new PhotoRepository();
		$repo->populateDates($limit);
	}

	public function getFromNasa(Request $request)
	{
		if ($request->request->has('date') && $request->get('date') !== '') {
			$date = $request->get('date');
		} else {
			$date = date('ymd');
		}

		if ($request->request->has('get_image') && $request->get('get_image') === 'false') {
			$getImage = false;
		} else {
			$getImage = true;
		}

		if ($request->request->has('old') && $request->get('old') === 'true') {
			$old = true;
		} else {
			$old = false;
		}

		Nasa::getInfoByDate($date, $getImage, $old);
		Tools::log('Daily APOD cron (apod.vidry.ca)');
	}

	public function getNasaImages(Request $request)
	{
		$config = new Config();
		$env    = $config->getParameter('env');

		if ($env !== 'local') {
			header('Location: /');
			exit();
		} else {
			$showForm = true;

			if ($request->request->has('year') && $request->get('year') !== '') {
				$year = (int)$request->get('year');
			} else {
				$year = (int)date('Y');
			}

			if ($request->request->has('month') && $request->get('month') !== '') {
				$month = (int)$request->get('month');
			} else {
				$month = 1;
			}

			if ($request->request->has('day') && $request->get('day') !== '') {
				$day = (int)$request->get('day');
			} else {
				$day = 1;
			}

			$this->viewParameters['year']  = $year;
			$this->viewParameters['month'] = $month;
			$this->viewParameters['day']   = $day;

			if ($showForm) {
				$this->viewParameters['current_year'] = date('Y');
				$this->viewParameters["latest_file"]  = $this->getLatestFile();
				$tmp      = preg_split('/\./', $this->viewParameters['latest_file']);
				$nextDate = \DateTime::createFromFormat('Ymd', '20' . $tmp[0]);
				$nextDate->modify('+1 day');

				$this->viewParameters['next_date'] = $nextDate;
				$this->viewParameters['show_form'] = true;
			}
		}

		Views::display('tools/get-nasa-images', $this->viewParameters);

		//Nasa::getImagesByDate($year, $month, $day);
	}

	public function getNasaImage(Request $request)
	{
		if ($request->request->has('year') && $request->get('year') !== ''
			&& $request->request->has('month') && $request->get('month') !== ''
			&& $request->request->has('day') && $request->get('day') !== ''
		) {
			if ($request->request->has('limit')) {
				$limit = (int)$request->get('limit');
			} else {
				$limit = 0;
			}

			Nasa::getImagesByDate(
				$request->get('year'),
				$request->get('month'),
				$request->get('day'),
				$limit
			);
		}
	}

	public function postSocial()
	{
		if (Auth::isAdmin()) {
			Views::display('tools/post-social', $this->viewParameters);
		} else {
			header('Location: /');
		}
	}

	public function uploadImage(Request $request)
	{
		$config    = new Config();
		$photosDir = $config->getParameter('photos_directory');
		$filename  = $request->get('filename');
		$year      = substr($filename, 0, 2);

		if ($year > 90) {
			$year = '19' . $year;
		} else {
			$year = '20' . $year;
		}

		if (is_array($_FILES)) {
			if (is_uploaded_file($_FILES['photo']['tmp_name'])) {
				$sourcePath = $_FILES['photo']['tmp_name'];
				$photoPath  = $photosDir . $year . '/';
				$targetPath = $photoPath . $_FILES['photo']['name'];

				if (!is_dir($photoPath)) {
					mkdir($photoPath, 0775, true);
				}

				if (move_uploaded_file($sourcePath, $targetPath)) {
					$thumbnailPath = $config->getParameter('photos_directory')
						. $year . '/'
						. '_sfpg_data/thumb/';

					if (!is_dir($thumbnailPath)) {
						mkdir($thumbnailPath, 0775, true);
					}

					$thumbnailPath .= $filename;

					Image::generateImageThumbnail(
						$targetPath,
						$thumbnailPath,
						Image::THUMBNAIL_MAX_WIDTH,
						Image::THUMBNAIL_MAX_HEIGHT
					);

					$fileParts = preg_split('/\./', $filename);

					echo '{"success":"true", "date_str":"' . $fileParts[0] . '", "extension":"' . $fileParts[1] . '"}';
					exit;
				}
			}
		}

		echo '{"success":"false"}';
		exit;
	}

	public function like(Request $request)
	{
		if ($request->request->has('photo_id')) {
			$result = Photo::like((int)$request->get('photo_id'));

			echo '{"success":"' . $result . '"}';
		}
		exit;
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function sitemap(Request $request)
	{
		$repo   = new PhotoRepository();
		$config = new Config();

		//echo $request->get('year');die();
		$this->viewParameters['base_url']     = $config->getParameter('base_url');
		$this->viewParameters['current_year'] = date('Y');

		if ($request->request->has('year')) {
			$year                          = (int)$request->get('year');
			$this->viewParameters['pages'] = $repo->findAllByYear($year);
			$this->viewParameters['year']  = $year;
		}

		header("Content-Type: application/xml; charset=utf-8");

		Views::display('sitemap', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function rss(Request $request)
	{
		$repo   = new PhotoRepository();
		$config = new Config();
		$page   = 1;
		$limit  = 50;

		//echo $request->get('year');die();
		$this->viewParameters['base_url'] = $config->getParameter('base_url');

		if ($request->request->has('page')) {
			$page = (int)$request->get('page');
		}

		if ($request->request->has('limit')) {
			$limit = (int)$request->get('limit');
		}

		if ($limit > 500) {
			$limit = 500;
		}

		$this->viewParameters['pages'] = $repo->findLatests($page, $limit);

		header("Content-Type: application/xml; charset=utf-8");

		Views::display('rss', $this->viewParameters);
	}

	/**
	 * Backup
	 */
	public function backup()
	{
		try {
			$db = Db::getInstance();
			$db->backup();

			echo 'Database dumped successfully';
		} catch (\Exception $e) {
			echo 'The backup failed: ' . $e->getMessage();
		}
	}

	public function clearCache()
	{
		$cache = new Cache();
		$cache->removeAll();

		echo 'Cache cleared';

		exit;
	}

	public function checkImages(Request $request)
	{
		$repo   = new PhotoRepository();
		$year   = (int)$request->get('year');
		$photos = $repo->findAllByYear($year);

		foreach ($photos as $photo) {
			$filename = $photo->getProperty('filename');
			$video    = $photo->getProperty('video');

			if ($video === null || $video === '') {
				$path = 'C:/Users/messi/OneDrive/Pictures/Nasa/' . $year . '/' . $filename;

				if (!file_exists($path)) {
					$tmp = preg_split('/\./', $filename);
					$url = 'https://apod.nasa.gov/apod/ap' . $tmp[0] . '.html';
					echo '<a href="' . $url . '" target="_blank">' . $path . '</a><br>';
				}
			}
		}
	}

	public function download(Request $request)
	{
		if ($request->request->has('id')) {
			Photo::download((int)$request->get('id'));
			exit();
		} else {
			header('Location:/');
		}
	}

	public function translate(Request $request)
	{
		if ($request->request->has('text') && $request->get('text') !== '') {
			$result = Translator::translate($request->get('text'));
			$text   = $result->translation;
		} else {
			$text = '';
		}

		echo $text;
		exit();
	}

	private function getLatestFile()
	{
		$dir         = 'C:/Users/messi/OneDrive/Pictures/Nasa/' . date('Y') . '/';
		$lastMod     = 0;
		$lastModFile = '';

		if ($this->isDirEmpty($dir)) {
			$dir = 'C:/Users/messi/OneDrive/Pictures/Nasa/' . (date('Y') - 1) . '/';
		}

		foreach (scandir($dir) as $entry) {
			if (is_file($dir . $entry) && filectime($dir . $entry) > $lastMod) {
				$lastMod     = filectime($dir . $entry);
				$lastModFile = $entry;
			}
		}

		return $lastModFile;
	}

	/**
	 * @param $dir
	 * @return bool
	 */
	private function isDirEmpty($dir) {
		return (count(scandir($dir)) == 2);
	}
}