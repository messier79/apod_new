<?php

namespace Controllers;

use Models\Photo;
use Models\Year;
use Repositories\PhotoRepository;
use Repositories\TagRepository;
use Repositories\YearRepository;
use Services\Photo as PhotoService;
use Services\Tag;
use Services\Translator;
use Services\Validation;
use Symfony\Component\HttpFoundation\Request;

class PhotoController extends AuthenticatedController
{
	const ADMIN_GROUP_ID = 1;
	const USER_GROUP_ID = 2;

	public function __construct()
	{
		parent::__construct(true);

		$this->viewParameters = [];
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function list(Request $request)
	{
		if ($request->request->has('year') && is_numeric($request->get('year'))) {
			$year  = $request->get('year');
			$month = $request->get('month');
		} else {
			$year  = date('Y');
			$month = date('m');
		}

		$date = new \DateTime();
		$date->setDate((int)$year, (int)$month, 1);
		$previousMonth = $month - 1;
		$nextMonth     = $month + 1;
		$previousYear  = $year;
		$nextYear      = $year;

		if ($nextMonth === 13) {
			$nextMonth = 1;
			$nextYear  = $year + 1;
		}

		if ($previousMonth === 0) {
			$previousMonth = 12;
			$previousYear  = $year - 1;
		}

		if (is_numeric($year)) {
			$repo                           = new PhotoRepository();
			$this->viewParameters['photos'] = $repo->findByMonth($year, $month, true);
		}
		$this->viewParameters['date']           = $date;
		$this->viewParameters['next_year']      = $nextYear;
		$this->viewParameters['next_month']     = $nextMonth;
		$this->viewParameters['previous_year']  = $previousYear;
		$this->viewParameters['previous_month'] = $previousMonth;

		echo $this->twig->render('admin/photos/list.html.twig', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function form(Request $request)
	{
		$tagRepo = new TagRepository();

		if ($request->request->has('month')) {
			$year  = $request->get('year');
			$month = $request->get('month');
		} else {
			$year  = date('Y');
			$month = date('m');
		}

		if ($request->request->has('id') && $request->get('id') !== 'add') {
			$photo = new Photo((int)$request->get('id'));

			if ((int)$request->get('id') === 0) {
				$photo->setProperty('id', $request->get('id'))
					  ->setProperty('filename', $request->get('filename'))
					  ->setProperty('video', $request->get('video'))
					  ->setProperty('title_fr', $request->get('title_fr'))
					  ->setProperty('title_en', $request->get('title_en'))
					  ->setProperty('description_fr', $request->get('description_fr'))
					  ->setProperty('description_en', $request->get('description_en'))
					  ->setProperty('credits', $request->get('credits'))
					  ->setProperty('status', $request->get('status'));
			} else {
				if ($request->request->has('translate') && $request->get('translate') == 1) {
					$translation = Translator::translate($photo->getProperty('title_en'));
					$photo->setProperty('title_fr', $translation->translation);
					$translation = Translator::translate(strip_tags($photo->getProperty('description_en')));
					$photo->setProperty('description_fr', $translation->translation);
				}
			}

			$year      = new Year($photo->getProperty('year_id'));
			$parts     = preg_split('/\./', $photo->getProperty('filename'));
			$date      = \DateTime::createFromFormat('ymd', $parts[0]);
			$dateStr   = $parts[0];
			$extension = $parts[1];
			$month     = $date->format('m');

			$this->viewParameters['date']       = $date;
			$this->viewParameters['photo_tags'] = $tagRepo->findByPhoto(($request->get('id')));
		} else {
			if ($request->request->has('date_str')) {
				$dateStr = $request->get('date_str');
			} else {
				$dateStr = date('ymd');
			}
			$photo = new Photo();
			$photo->setProperty('id', 0)
				  ->setProperty('filename', $dateStr . '.jpg')
				  ->setProperty('video', '')
				  ->setProperty('title_fr', '')
				  ->setProperty('title_en', '')
				  ->setProperty('description_fr', '')
				  ->setProperty('description_en', '')
				  ->setProperty('credits', '')
				  ->setProperty('status', 'enabled');
			$repo      = new YearRepository();
			$year      = $repo->findByName($year);
			$extension = 'jpg';

			$this->viewParameters['photo_tags'] = [];
		}

		$params                       = [
			'sortBy'    => 'tag_en',
			'sortOrder' => 'ASC',
		];
		$this->viewParameters['tags'] = $tagRepo->fetchAll($params);

		$this->viewParameters['date_str']                = $dateStr;
		$this->viewParameters['extension']               = $extension;
		$this->viewParameters['photo']                   = $photo;
		$this->viewParameters['year']                    = $year;
		$this->viewParameters['month']                   = $month;
		$this->viewParameters['title_en_stripped']       = strip_tags($photo->getProperty('title_en'));
		$this->viewParameters['description_en_stripped'] = strip_tags($photo->getProperty('description_en'));

		if ($request->request->has('errors')) {
			$this->viewParameters['errors'] = $request->get('errors');
		}

		echo $this->twig->render('admin/photos/form.html.twig', $this->viewParameters);
	}

	/**
	 * @param Request $request
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function save(Request $request)
	{
		$errors = Validation::validateClassParameters(Photo::class, $request->request->all());

		if (count($errors) === 0) {
			$id      = (int)$request->get('id');
			$repo    = new YearRepository();
			$year    = $repo->findByName($request->get('year_id'));
			$dateStr = \Services\Photo::getDateFromFilename($request->get('filename'));
			$photo   = new Photo($id);
			$photo->setProperty('year_id', $year->getId())
				  ->setProperty('date', $dateStr)
				  ->setProperty('filename', $request->get('filename'))
				  ->setProperty('video', $request->get('video'))
				  ->setProperty('title_fr', $request->get('title_fr'))
				  ->setProperty('title_en', $request->get('title_en'))
				  ->setProperty('description_fr', $request->get('description_fr'))
				  ->setProperty('description_en', $request->get('description_en'))
				  ->setProperty('credits', $request->get('credits'))
				  ->setProperty('status', $request->get('status'));

			if ($id === 0) {
				$photo->setProperty('created_at', date('Y-m-d H:i:s'));
			}

			if (count($errors) === 0) {
				$photo->save();

				if ($request->request->has('tag') && $request->get('tag') !== '') {
					$tags = preg_split('/,/', $request->get('tag'));

					Tag::addPhotoTags($photo, $tags);
				}
			}
		}

		if (count($errors) === 0) {
			$parts = preg_split('/\./', $request->get('filename'));
			$date  = \DateTime::createFromFormat('ymd', $parts[0]);
			$date->modify('+1 day');

			// Checks if next record exists
			$photo = PhotoService::getPhoto($date);
			$request->request->set('success', ['saved']);
			$request->request->set('date_str', $date->format('ymd'));

			if ($photo instanceof Photo) {
				$request->request->set('id', $photo->getId());
			} else {
				$request->request->set('id', 'add');
			}
			$this->form($request);
		} else {
			$request->request->set('errors', $errors);
			$this->form($request);
		}
	}

	/**
	 * @param Request $request
	 * @return void
	 * @throws \ReflectionException
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function delete(Request $request)
	{
		$photo = new Photo((int)$request->get('id'));
		$photo->setProperty('deleted_at', new \DateTime())
			  ->save();

		$this->list($request);
	}
}