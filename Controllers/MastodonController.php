<?php

namespace Controllers;

use Config\Config;
use Helpers\Tools;
use Services\Mastodon;
use Symfony\Component\HttpFoundation\Request;

class MastodonController extends DefaultController
{
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var array[]
	 */
	protected $viewParameters;

	public function __construct(bool $fromCron = false)
	{
		parent::__construct();
	}

	/**
	 * @param Request $request
	 */
	public function dailyPost(Request $request)
	{
		if ($request->request->has('lang')) {
			$lang = $request->get('lang');
		} else {
			$lang = 'en';
		}

		$config  = new Config();
		$baseUrl = $config->getParameter('base_url');
		$url     = $baseUrl . 'day/' . date('ymd') . '/' . $lang;

		if ($lang === 'fr') {
			$content = '#imageastrodujour #apod ';
		} else {
			$content = '#astronomypictureoftheday #apod ';
		}

		$content .= $url;

		Tools::log('Daily Mastodon cron (apod.vidry.ca) - ' . $lang);

		Mastodon::post($content, $lang);
	}
}
