<?php

namespace Helpers;

class Image
{
	const THUMBNAIL_MAX_WIDTH = 160;
	const THUMBNAIL_MAX_HEIGHT = 160;
	const MONTH_THUMBNAIL_CROP_X = 414;
	const MONTH_THUMBNAIL_CROP_Y = 98;
	const MONTH_THUMBNAIL_CROP_WIDTH = 1080;
	const MONTH_THUMBNAIL_CROP_HEIGHT_6 = 880;
	const MONTH_THUMBNAIL_CROP_HEIGHT_5 = 733;
	const MONTH_THUMBNAIL_CROP_HEIGHT_4 = 585;
	const MONTH_THUMBNAIL_WIDTH = 300;
	const YEAR_MOSAIC_CROP_X = 136;
	const YEAR_MOSAIC_CROP_Y = 98;
	const YEAR_MOSAIC_CROP_WIDTH = 1080;
	const YEAR_MOSAIC_CROP_HEIGHT = 7522;


	/**
	 * @param string $sourcePath
	 * @param string $thumbnailPath
	 * @param int|null $maxWidth
	 * @param int|null $maxHeight
	 * @return bool
	 */
	public static function generateImageThumbnail(
		string $sourcePath,
		string $thumbnailPath,
		int $maxWidth = null,
		int $maxHeight = null
	): bool
	{
		if (@exif_imagetype($sourcePath)) {
			list($sourceWidth, $sourceHeight, $sourceType) = getimagesize($sourcePath);
			$sourceGdImage = false;
			$orientation = self::getOrientation($sourcePath);

			if ($maxWidth === null) {
				$maxWidth = self::THUMBNAIL_MAX_WIDTH;
			}

			if ($maxHeight === null) {
				$maxHeight = self::THUMBNAIL_MAX_HEIGHT;
			}

			switch ($sourceType) {
				case IMAGETYPE_GIF:
					$sourceGdImage = imagecreatefromgif($sourcePath);
					break;
				case IMAGETYPE_JPEG:
					$sourceGdImage = imagecreatefromjpeg($sourcePath);
					break;
				case IMAGETYPE_PNG:
					$sourceGdImage = imagecreatefrompng($sourcePath);
					break;
			}

			if ($sourceGdImage === false) {
				return false;
			}

			$sourceAspectRatio    = $sourceWidth / $sourceHeight;
			$thumbnailAspectRatio = $maxWidth / $maxHeight;

			if ($sourceWidth <= $maxWidth && $sourceHeight <= $maxHeight) {
				$thumbnailImageWidth  = $sourceWidth;
				$thumbnailImageHeight = $sourceHeight;
			} elseif ($thumbnailAspectRatio > $sourceAspectRatio) {
				$thumbnailImageWidth  = (int)($maxHeight * $sourceAspectRatio);
				$thumbnailImageHeight = $maxHeight;
			} else {
				$thumbnailImageWidth  = $maxWidth;
				$thumbnailImageHeight = (int)($maxWidth / $sourceAspectRatio);
			}

			$thumbnailGdImage = imagecreatetruecolor($thumbnailImageWidth, $thumbnailImageHeight);
			imagecopyresampled($thumbnailGdImage, $sourceGdImage, 0, 0, 0, 0, $thumbnailImageWidth, $thumbnailImageHeight, $sourceWidth, $sourceHeight);

			$imgDisp   = imagecreatetruecolor($thumbnailImageWidth, $thumbnailImageHeight);
			$backcolor = imagecolorallocatealpha($imgDisp, 0, 0, 0, 127);
			imagefill($imgDisp, 0, 0, $backcolor);
			imagesavealpha($imgDisp, true);

			imagecopy($imgDisp, $thumbnailGdImage, (imagesx($imgDisp) / 2) - (imagesx($thumbnailGdImage) / 2), (imagesy($imgDisp) / 2) - (imagesy($thumbnailGdImage) / 2), 0, 0, imagesx($thumbnailGdImage), imagesy($thumbnailGdImage));

			// Rotates if necessary
			if ($orientation > 0) {
				$imgDisp = imagerotate($imgDisp, $orientation, 0);
			}

			imagealphablending($imgDisp, false);
			imagesavealpha($imgDisp, true);
			imagepng($imgDisp, $thumbnailPath);

			imagedestroy($sourceGdImage);
			imagedestroy($thumbnailGdImage);
			imagedestroy($imgDisp);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Resize image - preserve ratio of width and height.
	 * @param string $sourceImage path to source JPEG image
	 * @param string $targetImage path to final JPEG image file
	 * @param int $maxWidth maximum width of final image (value 0 - width is optional)
	 * @param int $maxHeight maximum height of final image (value 0 - height is optional)
	 * @param int $quality quality of final image (0-100)
	 * @return bool
	 */
	public static function resize(
		string $sourceImage,
		string $targetImage,
		int $maxWidth,
		int $maxHeight,
		int $quality = 80
	)
	{
		// Obtain image from given source file.
		if (!$image = imagecreatefromjpeg($sourceImage)) {
			return false;
		}

		// Get dimensions of source image.
		[$origWidth, $origHeight] = getimagesize($sourceImage);

		if ($maxWidth == 0) {
			$maxWidth = $origWidth;
		}

		if ($maxHeight == 0) {
			$maxHeight = $origHeight;
		}

		// Calculate ratio of desired maximum sizes and original sizes.
		$widthRatio  = $maxWidth / $origWidth;
		$heightRatio = $maxHeight / $origHeight;

		// Ratio used for calculating new image dimensions.
		$ratio = min($widthRatio, $heightRatio);

		// Calculate new image dimensions.
		$newWidth  = (int)$origWidth * $ratio;
		$newHeight = (int)$origHeight * $ratio;

		// Create final image with new dimensions.
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($newImage, $targetImage, $quality);

		// Free up the memory.
		imagedestroy($image);
		imagedestroy($newImage);

		return true;
	}
	
	/**
	 * Crop image
	 * @param string $sourceImage path to source JPEG image
	 * @param string $targetImage path to final JPEG image file
	 * @param int $width width of cropped rectangle
	 * @param int $height height of cropped rectangle
	 * @param int $x X position of cropped rectangle
	 * @param int $y Y position of cropped rectangle
	 * @param bool $destroySource
	 * @return bool
	 */
	public static function crop(
		string $sourceImage,
		string $targetImage,
		int $width,
		int $height,
		int $x = 0,
		int $y = 0,
		bool $destroySource = true
	)
	{
		$im = imagecreatefrompng($sourceImage);
		$size = min(imagesx($im), imagesy($im));
		$options = [
			'x' => $x,
			'y' => $y,
			'width' => $width,
			'height' => $height,
		];
		$im2 = imagecrop($im, $options);
		
		if ($im2 !== false) {
			imagepng($im2, $targetImage);
			imagedestroy($im2);
		}

		imagedestroy($im);

		if ($destroySource) {
			unlink($sourceImage);
		}
		
		return true;
	}

	/**
	 * @param string $filename
	 * @return int
	 */
	public static function getOrientation(string $filename): int
	{
		$deg = 0;

		// Do we have the ability to rotate?
		if (function_exists('exif_read_data')) {
			// Does the image have rotation set?
			$exif = @exif_read_data($filename);

			if($exif && isset($exif['Orientation'])) {
				$orientation = $exif['Orientation'];

				// Does it actually need rotation
				if($orientation != 1){
					switch ($orientation) {
						case 3:
							$deg = 180;
							break;
						case 6:
							$deg = 270;
							break;
						case 8:
							$deg = 90;
							break;
					}
				}
			}
		}
		return $deg;
	}
	
	public static function downloadImage(string $url, string $path, string $date = null)
	{
		if ($date === null) {
			$dateObj = new \DateTime();
			$date = $dateObj->format('ymd');
		}

		$options = [
			CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
			CURLOPT_POST           =>false,        //set to GET
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
		];
		$ch = curl_init ($url);
		//curl_setopt_array( $ch, $options );
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$rawdata = curl_exec($ch);
		curl_close ($ch);

		if(file_exists($path)){
			unlink($path);
		}
		
		$fp = fopen($path,'x');
		fwrite($fp, $rawdata);
		fclose($fp);
	}
}
