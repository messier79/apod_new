<?php

namespace Helpers;

use Controllers\IndexController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Routing
 * @package Helpers
 */
class Routing
{
	/**
	 * Handles the route
	 */
	public static function handle()
	{
		try {
			$requestUri = $_SERVER['REQUEST_URI'];
			$routes     = new \Config\Routing();

			foreach ($routes->getRoutes() as $route) {
				$pathParts       = preg_split('/\//', $route->getPath());
				$requestUriParts = preg_split('/\//', $requestUri);
				$parameters      = [];
				$cleanRequestUri = '';
				$nakedPath       = '';
				$hasVars         = false;

				foreach ($pathParts as $key => $part) {
					$isVar = strpos($part, '{');

					if ($part !== '') {
						if ($isVar === false) {
							$nakedPath       .= '/' . $part;

							if (isset($requestUriParts[$key])) {
								$cleanRequestUri .= '/' . $requestUriParts[$key];
							}
						} else {
							$name             = preg_replace('/{/', '', $part);
							$name             = preg_replace('/}/', '', $name);
							$parameters[$key] = $name;
							$hasVars          = true;
						}
					}
				}

				if ($cleanRequestUri !== ''
					&& (($hasVars === false && $requestUri === $nakedPath)
						|| $hasVars === true && $cleanRequestUri === $nakedPath)
				) {
					$parts       = preg_split('/::/', $route->getDefault('_controller'));
					$controller  = new $parts[0];
					$method      = $parts[1];
					$paramsArray = [];
					$request     = new Request();

					foreach ($parameters as $key => $parameter) {
						if (isset($requestUriParts[$key])) {
							$paramsArray[$parameter] = $requestUriParts[$key];
						}
					}
					foreach ($_POST as $key => $value) {
						$paramsArray[$key] = $value;
					}

					$request->request->add($paramsArray);
					$request->files->add($_FILES);

					$controller->$method($request);

					return true;
				}
			}

			// Fallback, displays the homepage
			$controller = new IndexController();
			$request = new Request();
			$controller->index($request);
		} catch (\Exception $e) {
			echo $e->getMessage()
			. '<br>' . $e->getFile()
			. '<br>' . $e->getLine();
		}
	}
}