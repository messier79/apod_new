<?php

namespace Helpers;

class File
{
	/**
	 * @param string $path
	 * @return array
	 */
	public static function getList(string $path): array
	{
		return array_diff(scandir($path), ['.', '..']);
	}
}
