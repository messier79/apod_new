<?php
namespace Helpers;

use Config\Config;

class Middleware
{
	/**
	 * Middleware constructor.
	 */
	public function __construct()
	{
		// set default timezone
		date_default_timezone_set('America/Montreal');
		setlocale(LC_MONETARY, 'en_US.UTF-8');
		
		Session::start();

		$config = new Config();
	}
}
