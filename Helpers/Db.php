<?php

namespace Helpers;

use Config\Config;
use Doctrine\DBAL\Statement;
use Exception;
use Helpers\Session;
use PDO;

class Db
{
	private static $_instance;
	
	/**
	 * @var Statement
	 */
	protected $stmt;

	/**
	 * @var PDO
	 */
	protected $pdo;
	
	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Db constructor.
	 */
	public function __construct()
	{
		try {
			$dbSettings = Session::get('db_settings');

			if ($dbSettings['engine'] === 'sqlsrv') {
				$dsn = "sqlsrv:Server=" . $dbSettings['host'] . ";Database=" . $dbSettings['name'] . "";
			} else {
				$dsn = "mysql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['name'] . ";charset=" . $dbSettings['charset'];
			}
			$this->pdo = new PDO($dsn, $dbSettings['user'], $dbSettings['pass']);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			$this->pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * @param string $sql
	 * @param array $params
	 * @return bool
	 */
	public function query(string $sql, array $params = [])
	{
		try {
			$this->stmt = $this->pdo->prepare($sql);

			foreach ($params as $key => $value) {
				$this->stmt->bindValue(':' . $key, $value);
			}
			$this->stmt->execute();

			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * @return mixed
	 */
	public function fetch()
	{
		return $this->stmt->fetch();
	}

	/**
	 * @return array
	 */
	public function fetchAll()
	{
		return $this->stmt->fetchAll();
	}

	public function getLastInsertId()
	{
		return $this->pdo->lastInsertId();
	}

	public function backup()
	{
		$config  = new Config();
		$host = $config->getParameter('host');
		$user = $config->getParameter('user');
		$pass = $config->getParameter('pass');
		$name = $config->getParameter('name');
		$path = $config->getParameter('backup_path');
		$dateStr = date("Ymd_His");

		if (!is_dir($path)) {
			mkdir($path, 0755, true);
		}

		$cmd = "mysqldump -h {$host} -u {$user} -p{$pass} {$name} > " . $path . "{$dateStr}_{$name}.sql";

		exec($cmd);
	}

	public function restore(string $filename)
	{
		$config  = new Config();
		$host = $config->getParameter('host');
		$user = $config->getParameter('user');
		$pass = $config->getParameter('pass');
		$name = $config->getParameter('name');
		$path = $config->getParameter('backup_path');

		$cmd = "mysql -h {$host} -u {$user} -p{$pass} {$name} < " . $path . $filename;

		exec($cmd);
	}
}