<?php

namespace Helpers;

/**
 * Class Session
 * @package Helpers
 */
class Session
{
	/**
	 * @var bool
	 */
	private static $isStarted = false;

	/**
	 * Session constructor.
	 */
	public function __construct()
	{
		self::start();
	}

	/**
	 * Starts the Session
	 */
	public static function start()
	{
		if (!self::$isStarted && session_status() === 1) {
			self::$isStarted = true;
			session_start();
		}
	}

	/**
	 * Destroys the Session
	 */
	public static function destroy()
	{
		session_destroy();
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public static function has(string $key)
	{
		return isset($_SESSION[$key]);
	}

	/**
	 * @param string $key
	 * @param null $defaultValue
	 * @return mixed|null
	 */
	public static function get(string $key, $defaultValue = null)
	{
		return ((isset($_SESSION[$key])) ? ($_SESSION[$key]) : ($defaultValue));
	}

	/**
	 * @param string $key
	 * @param $value
	 */
	public static function set(string $key, $value)
	{
		$_SESSION[$key] = $value;
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public static function remove(string $key)
	{
		unset($_SESSION[$key]);
	}

	public static function batchRemove(array $keys)
	{
		foreach ($_SESSION as $key => $val) {
			if (in_array($key, $keys)) {
				unset($_SESSION[$key]);
			}
		}
	}
}
