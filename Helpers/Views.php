<?php

namespace Helpers;

use Config\Config;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use voku\cache\Cache;

/**
 * Class Views
 * @package Helpers
 */
class Views
{
	/**
	 * @var string
	 */
	const VIEW_PATH = __DIR__ . '/../Resources/views';

	/**
	 * @var string
	 */
	const TWIG_EXTENSION = '.html.twig';

	/**
	 * @param string $name
	 * @param array $parameters
	 * @param bool $display
	 * @return mixed|string|null
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public static function display(string $name, array $parameters, bool $display = true)
	{
		$twig     = self::getTwig();
		$template = self::getTemplateName($name, $parameters);
		$config = new Config();
		$debugMode = $config->getParameter('twig_debug');
		$cache = new Cache();
		$cacheKey = 'template_form_' . $template . '_' . serialize($parameters);

		if ($debugMode || !$cache->existsItem($cacheKey)) {
			$output = $twig->render($template, $parameters);
			$cache->setItem($cacheKey, $output);
		} else {
			$output = $cache->getItem($cacheKey);
		}

		if ($display === true) {
			echo $output;
		} else {
			return $output;
		}
	}

	/**
	 * @return Environment
	 */
	public static function getTwig(): Environment
	{
		$config = new Config();
		$loader = new FilesystemLoader(self::VIEW_PATH);
		$twig   = new Environment($loader, ['debug' => $config->getParameter('twig_debug')]);
		$twig->enableDebug();
		$twig->enableAutoReload();

		if ($config->getParameter('twig_debug')) {
			$twig->addExtension(new DebugExtension());
		}

		return $twig;
	}

	/**
	 * @param string $name
	 * @param array $parameters
	 * @param bool $withExtension
	 * @return string
	 */
	public static function getTemplateName(string $name, array $parameters, bool $withExtension = true)
	{
		$template   = $name;

		if ($withExtension === true) {
			$template .= self::TWIG_EXTENSION;
		}

		return $template;
	}

	/**
	 * @param string $name
	 * @param array $parameters
	 * @return bool
	 */
	public static function hasTemplateOverride(string $name, array $parameters): bool
	{
		$path       = self::VIEW_PATH;
		$domainPath = $path . '/domains/' . strtolower($parameters['settings']['domain']);
		$domainPath = self::addSubDirectory($domainPath);

		if (file_exists($domainPath . '/' . $name . self::TWIG_EXTENSION)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param string $name
	 * @param array $parameters
	 * @return bool
	 */
	public static function templateExists(string $name, array $parameters): bool
	{
		$path       = self::VIEW_PATH;
		$domainPath = $path . '/domains/' . strtolower($parameters['settings']['domain']);

		if (file_exists($path . '/' . $name . self::TWIG_EXTENSION)
			|| file_exists($domainPath . '/' . $name . self::TWIG_EXTENSION)
		) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param string $path
	 * @return string
	 */
	private static function addSubDirectory(string $path): string
	{
		if (Session::has('subdirectory')) {
			$path .= Session::get('subdirectory');
		}

		return $path;
	}
}