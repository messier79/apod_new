<?php

namespace Helpers;

use Config\Config;
use Models\Album;
use PHPMailer\PHPMailer\PHPMailer;
use Repositories\PhotoRepository;
use SendGrid\Content;
use SendGrid\Email;
use SendGrid\Mail;
use Services\AbTest;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Tools
 * @package Helpers
 */
class Tools
{
	const HASH_ALGO = 'sha512';
	const STORAGE_PATH = __DIR__ . '/../storage/';

	public static function hash(string $string): string
	{
		return hash(self::HASH_ALGO, md5($string));
	}

	public static function redirectOldUrls()
	{
		$url   = $_SERVER['REQUEST_URI'];
		$parts = preg_split('/\/ap/', $url);

		if (count($parts) === 2) {
			$parts = preg_split('/\.html/', $parts[1]);

			if (count($parts) === 2) {
				$url = '/day/' . $parts[0];
				header('Location: ' . $url);
				exit;
			}
		}
	}

	public static function handleFileTypes()
	{
		$url         = $_SERVER['REQUEST_URI'];
		$script      = basename($url);
		$scriptParts = preg_split('/\./', $script);

		if (self::getFilePath($url) !== null) {
			self::setHeaders($url);
			$file = self::getFilePath($url);

			if (substr($url, -4) == '.php') {
				header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
				include($file);
				exit;
			} else {
				self::sendResponseHeaders($file);
			}
		} elseif (count($scriptParts) > 1) {
			self::setHeaders($url);

			if (self::getFilePath($url) !== null) {
				self::sendResponseHeaders(self::getFilePath($url));
			}
		}
	}

	private static function setHeaders(string $script)
	{
		$contentType = '';

		if (substr($script, -4) == '.css') {
			$contentType = 'text/css';
		} else if (substr($script, -3) == '.js') {
			$contentType = 'application/javascript';
		} else if (substr($script, -4) == '.jpg') {
			$contentType = 'image/jpeg';
		} else if (substr($script, -4) == '.png') {
			$contentType = 'image/png';
		} else if (substr($script, -4) == '.svg') {
			$contentType = 'image/svg+xml';
		} else if (substr($script, -4) == '.gif') {
			$contentType = 'image/gif';
		} else if (substr($script, -4) == '.ico') {
			$contentType = 'image/x-icon';
		}

		header('Content-Type: ' . $contentType);
		header('X-Content-Type-Options: nosniff');
	}

	/**
	 * @param string $filename
	 * @return string|null
	 */
	private static function getFilePath(string $filename)
	{
		$assetsPath = __DIR__ . '/../Resources/assets' . $filename;

		if (file_exists($assetsPath) && !is_dir($assetsPath)) {
			return $assetsPath;
		} else {
			return null;
		}
	}

	private static function sendResponseHeaders(string $file)
	{
		header("Content-Length: " . filesize($file));
		header("Cache-Control: public, max-age=604800, pre-check=604800");
		header("Pragma: public");
		header("Expires: " . date(DATE_RFC822, strtotime("1 week")));
		$last_modified_time = filemtime($file);
		$etag               = md5_file($file);
		header("Last-Modified: " . gmdate("D, d M Y H:i:s", $last_modified_time) . " GMT");
		header("Etag: $etag");

		if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time or (isset($_SERVER['HTTP_IF_NONE_MATCH']) && (stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) == $etag))) {
			header("HTTP/1.1 304 Not Modified");
			exit;
		} else {
			header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
			$file_handle = fopen($file, 'rb');
			fpassthru($file_handle);
		}
		exit;
	}

	/**
	 * @param $val
	 * @return bool
	 */
	public static function isDecimal($val): bool
	{
		return is_numeric($val) && floor($val) != $val;
	}

	/**
	 * @param string $input
	 * @param string $separator
	 * @return string
	 */
	public static function camelize(string $input, string $separator = '_')
	{
		$camelized = str_replace($separator, '', ucwords($input, $separator));

		return lcfirst($camelized);
	}

	/**
	 * @param string $comments
	 * @return array
	 */
	public static function extractAnnotationsFromComments(string $comments): array
	{
		$annotations = [];
		$pattern     = "#(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#";
		preg_match_all($pattern, $comments, $matches, PREG_PATTERN_ORDER);

		foreach ($matches[0] as $match) {
			$parts              = preg_split('/ /', $match);
			$annotationPosition = strpos($parts[0], '@');

			if ($annotationPosition === 0) {
				$name               = preg_replace('/\\t/', '', $parts[0]);
				$name               = preg_replace('/\s+/', '', $name);
				$name               = preg_replace('/\\r/', '', $name);
				$annotations[$name] = [];

				if (isset($parts[1]) && $name === '@var') {
					$types = preg_split('/\|/', $parts[1]);

					foreach ($types as &$type) {
						$type = preg_replace('/\\r/', '', $type);
					}
					$annotations[$name]['types'] = $types;
				}

				if (count($parts) > 1) {
					$annotations[$name]['options'] = '';

					for ($i = 1; $i < count($parts); $i++) {
						if ($annotations[$name]['options'] !== '') {
							$annotations[$name]['options'] .= ' ';
						}
						$value                         = preg_replace('/\\r/', '', $parts[$i]);
						$annotations[$name]['options'] .= $value;
					}
				}
			}
		}

		return $annotations;
	}

	public static function arrayToCsv(
		array $array,
		string $filename = "export.csv",
		string $delimiter = ","
	)
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '";');

		$f = fopen('php://output', 'w');

		foreach ($array as $line) {
			fputcsv($f, $line, $delimiter);
		}

		fclose($f);
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public static function removeAccents(string $string): string
	{
		if (!preg_match('/[\x80-\xff]/', $string))
			return $string;

		$chars = [
			// Decompositions for Latin-1 Supplement
			chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
			chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
			chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
			chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
			chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
			chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
			chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
			chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
			chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
			chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
			chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
			chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
			chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
			chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
			chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
			chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
			chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
			chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
			chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
			chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
			chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
			chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
			chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
			chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
			chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
			chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
			chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
			chr(195) . chr(191) => 'y',
			// Decompositions for Latin Extended-A
			chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
			chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
			chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
			chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
			chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
			chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
			chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
			chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
			chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
			chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
			chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
			chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
			chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
			chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
			chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
			chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
			chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
			chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
			chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
			chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
			chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
			chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
			chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
			chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
			chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
			chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
			chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
			chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
			chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
			chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
			chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
			chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
			chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
			chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
			chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
			chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
			chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
			chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
			chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
			chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
			chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
			chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
			chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
			chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
			chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
			chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
			chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
			chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
			chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
			chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
			chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
			chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
			chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
			chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
			chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
			chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
			chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
			chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
			chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
			chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
			chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
			chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
			chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
			chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
		];

		$string = strtr($string, $chars);

		return $string;
	}

	public static function removeSpecialCharacters(string $string): string
	{
		$string = self::removeAccents($string);
		$string = preg_replace('/\?/', '-', $string);
		$string = preg_replace('/ /', '-', $string);
		$string = preg_replace('/:/', '-', $string);
		$string = preg_replace('/&/', '-', $string);
		$string = preg_replace('/,/', '-', $string);
		$string = preg_replace('/!/', '-', $string);
		$string = preg_replace('/\'/', '-', $string);

		return $string;
	}

	/**
	 * @return bool
	 * @var Album $album
	 */
	public static function checkImageExists(Album $album): bool
	{
		$config = new Config();
		$path   = $config->getParameter('albums_directory')
			. $album->getProperty('directory') . '/'
			. $album->getProperty('cover');

		if (!file_exists($path)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param int|null $year
	 * @param int|null $month
	 * @param int|null $day
	 * @return array
	 */
	public static function export(int $year = null, int $month = null, int $day = null): array
	{
		$repo = new PhotoRepository();
		$data = ['photos'];

		if ($year !== null && $month !== null && $day !== null) {
			$date = new \DateTime();
			$date->setDate($year, $month, $day);

			$photo = $repo->findByDate($date);
			$data['photos'] = [
				$year => [
					$month => [
						$day => $photo->__toArray(),
					],
				],
			];
		} elseif ($year !== null && $month !== null) {
			$photos = $repo->findByMonth($year, $month);
			$data['photos'] = [
				$year => [
					$month => [],
				],
			];

			foreach ($photos as $photo) {
				$parts = preg_split('/\./', $photo->getProperty('filename'));
				$date  = \DateTime::createFromFormat('ymd', $parts[0]);

				if ($date !== false) {
					$day = $date->format('j');

					$data['photos'][$year][$month][$day] = $photo->__toArray();
				}
			}
		} elseif ($year !== null) {
			$photos = $repo->findByYear($year);
			$data['photos'] = [
				$year => [],
			];

			foreach ($photos as $photo) {
				$parts = preg_split('/\./', $photo->getProperty('filename'));
				$date  = \DateTime::createFromFormat('ymd', $parts[0]);

				if ($date !== false) {
					$month = $date->format('n');
					$day   = $date->format('j');

					$data['photos'][$year][$month][$day] = $photo->__toArray();
				}
			}
		}

		return $data;
	}

	/**
	 * @param int $length
	 * @return string
	 */
	public static function generateRandomString(int $length = 10): string
	{
		$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString     = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}

	public static function objectToArray($obj)
	{
		//only process if it's an object or array being passed to the function
		if(is_object($obj) || is_array($obj)) {
			$ret = (array) $obj;
			foreach($ret as &$item) {
				//recursively process EACH element regardless of type
				$item = self::objectToArray($item);
			}
			return $ret;
		}
		//otherwise (i.e. for scalar values) return without modification
		else {
			return $obj;
		}
	}

	/**
	 * @param string $email
	 * @param string $name
	 * @param string $subject
	 * @param string $message
	 * @return bool
	 */
	public static function sendEmail(string $email, string $name, string $subject, string $message): bool
	{
		$config = new Config();
		$mail   = new PHPMailer(true);

		try {
			//Server settings
			$mail->isSMTP();
			//$mail->SMTPDebug  = 2;
			$mail->Host       = $config->getParameter('smtp_host');
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = $config->getParameter('smtp_secure');
			$mail->Username   = $config->getParameter('smtp_user');
			$mail->Password   = $config->getParameter('smtp_pass');
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port       = $config->getParameter('smtp_port');

			//Recipients
			$mail->setFrom($email, $name);
			$mail->addAddress($config->getParameter('contact_recipient'));

			// Content
			$mail->Subject = $subject;
			$mail->Body    = $message;

			$mail->send();

			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * @param string $message
	 * @return void
	 */
	public static function log(string $message)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://api.vidry.ca/log/42a0bffc-e897-47bc-80fc-a127ff65c62a");
		curl_setopt($ch, CURLOPT_POST, 1);

		// In real life you should use something like:
		curl_setopt($ch, CURLOPT_POSTFIELDS,
			http_build_query([
				'payload' => $message,
			])
		);

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec($ch);

		curl_close($ch);
	}
}