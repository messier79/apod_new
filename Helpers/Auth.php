<?php

namespace Helpers;

use Models\User as UserEntity;
use Repositories\UserRepository;

class Auth
{
	/**
	 * @return bool
	 */
	public static function isAuthenticated(): bool
	{
		$isAuthenticated = false;

		if (Session::has('user')) {
			$isAuthenticated = true;
		} elseif (isset($_COOKIE['remember_me'])) {
			$cookie   = json_decode($_COOKIE['remember_me']);
			$expireAt = preg_replace('/\+/', ' ', $cookie->expire_at);
			$expireAt = new \DateTime($expireAt);
			$today    = new \DateTime();

			if ($expireAt > $today) {
				$repo            = new UserRepository();
				$user            = $repo->findByToken($cookie->token);
				$isAuthenticated = true;

				Session::set('user', $user);
			}
		}

		return $isAuthenticated;
	}

	/**
	 * @return bool
	 */
	public static function isAdmin(): bool
	{
		$repo = new UserRepository();

		if (!self::isAuthenticated()) {
			return false;
		}

		return $repo->isAdmin($_SESSION['user']);
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param bool $rememberMe
	 * @return bool
	 */
	public static function login(string $username, string $password, bool $rememberMe = false)
	{
		$repo = new UserRepository();
		$user = $repo->getUserByUsernameAndPassword($username, $password);

		if ($user === false) {
			return false;
		} else {
			Session::set('user', $user);

			if ($rememberMe) {
				self::setRememberMeCookie($user);
			}

			return true;
		}
	}

	/**
	 * @param UserEntity $user
	 */
	private static function setRememberMeCookie(UserEntity $user)
	{
		$expireAt = new \DateTime();
		$expireAt->add(new \DateInterval('P1Y'));
		$repo = new UserRepository();

		$data = [
			'token'     => $repo->getToken($user),
			'expire_at' => $expireAt->format('Y-m-d 00:00:00'),
		];
		setcookie('remember_me', json_encode($data), $expireAt->format('U'));
	}

	/**
	 * Removes a Session parameter
	 */
	public static function logout()
	{
		unset($_COOKIE['remember_me']);
		setcookie('remember_me', '', time() - 3600, '/');
		Session::remove('user');
	}
}