<?php

namespace Repositories;

use Helpers\Session;
use Models\Comment;

class CommentRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $params
	 * @return Comment[]
	 */
	public function fetchAll(array $params = []): array
	{
		$lang = Session::get('lang');

		if (isset($params['sortBy'])) {
			$sortBy = $params['sortBy'];
		} else {
			$sortBy = 'created_at';
		}

		if (isset($params['sortOrder'])) {
			$sortOrder = $params['sortOrder'];
		} else {
			$sortOrder = 'DESC';
		}

		$comments  = [];
		$sql   = "
			SELECT *
			FROM comments
			WHERE deleted_at IS NULL
			AND photo_id = :photo_id
			ORDER BY " . $sortBy . " " . $sortOrder . "
		";
		$params = [
			'photo_id' => $params['photo_id'],
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as $item) {
			$comment = new Comment($item['id']);
			$comments[$item['id']] = $comment;
		}

		return $comments;
	}

	/**
	 * @param int $photoId
	 * @param string $username
	 * @param string $comment
	 * @return void
	 */
	public function insert(int $photoId, string $username, string $comment)
	{
		$sql = "
			INSERT INTO comments (
			    photo_id,
			    username,
			    comment
			) VALUES (
			    :photo_id,
			    :username,
			    :comment
			)
		";
		$params = [
			'photo_id' => $photoId,
			'username' => $username,
			'comment'  => $comment,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @param int $id
	 * @return void
	 */
	public function delete(int $id)
	{
		$sql = "
			DELETE FROM comments
			WHERE id = :id
		";
		$params = [
			'id'   => $id,
		];
		$this->pdo->query($sql, $params);
	}
}
