<?php

namespace Repositories;

use Helpers\Tools;
use Models\Album;
use Models\Year;

class YearRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
		$this->className = '\\Models\\Year';
	}

	/**
	 * @param array $params
	 * @return Tag[]
	 */
	public function fetchAll(array $params = []): array
	{
		if (isset($params['sortBy'])) {
			$sortBy = $params['sortBy'];
		} else {
			$sortBy = 'tag_en';
		}

		if (isset($params['sortOrder'])) {
			$sortOrder = $params['sortOrder'];
		} else {
			$sortOrder = 'ASC';
		}

		$tags  = [];
		$sql   = "
			SELECT *
			FROM years
			WHERE deleted_at IS NULL
			ORDER BY " . $sortBy . " " . $sortOrder . "
		";
		$params = [];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as $item) {
			$tags[$item['id']] = new Year($item['id']);
		}

		return $tags;
	}

	/**
	 * @param int $year
	 * @return Year
	 */
	public function findByName(int $year): Year
	{
		$sql        = "
			SELECT id
			FROM years
			WHERE name = :year
		";
		$params = [
			'year' => $year,
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetch();

		return new Year($result['id']);
	}

	/**
	 * @return Year[]
	 */
	public function findActive(): array
	{
		$sql        = "
			SELECT id
			FROM years
			WHERE status = 'enabled'
			ORDER BY name
		";
		$this->pdo->query($sql);
		$results = $this->pdo->fetchAll();
		$years   = [];
		
		foreach ($results as $result) {
			$years[] = new Year($result['id']);
		}

		return $years;
	}
}