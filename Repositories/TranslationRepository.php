<?php

namespace Repositories;

use Models\Translation;

class TranslationRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
		$this->className = '\\Models\\Translation';
	}

	/**
	 * @param string $lang
	 * @param bool $returnObject
	 * @return Translation[]
	 */
	public function findByLang(string $lang, bool $returnObject = false): array
	{
		$sql        = "
			SELECT id, code, value
			FROM translations
			WHERE lang = :lang
		";
		$params = [
			'lang' => $lang,
		];
		$this->pdo->query($sql, $params);
		$items   = $this->pdo->fetchAll();
		$results = [];

		foreach ($items as $item) {
			if ($returnObject === true) {
				$results[] = new Translation($item['id']);
			} else {
				$results[$item['code']] = $item['value'];
			}
		}

		return $results;
	}

	/**
	 * @param string $lang
	 * @param string $code
	 * @return Translation|false
	 */
	public function findByLangAndCode(string $lang, string $code)
	{
		$sql        = "
			SELECT id, code, value
			FROM translations
			WHERE lang = :lang
			AND code = :code
		";
		$params = [
			'lang' => $lang,
			'code' => $code,
		];
		$this->pdo->query($sql, $params);

		return $this->pdo->fetch();
	}
}