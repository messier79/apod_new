<?php

namespace Repositories;

use Helpers\Session;
use Models\Tag;

class TagRepository extends Repository
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $params
	 * @return Tag[]
	 */
	public function fetchAll(array $params = []): array
	{
		if (Session::has('lang')) {
			$lang = Session::get('lang');
		} else {
			$lang = 'en';
		}

		if (isset($params['sortBy'])) {
			$sortBy = $params['sortBy'];
		} else {
			$sortBy = 'tag_en';
		}

		if (isset($params['sortOrder'])) {
			$sortOrder = $params['sortOrder'];
		} else {
			$sortOrder = 'ASC';
		}

		$tags  = [];
		$sql   = "
			SELECT *
			FROM tags
			WHERE deleted_at IS NULL
			ORDER BY " . $sortBy . " " . $sortOrder . "
		";
		$params = [];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as $item) {
			$tag               = new Tag($item['id']);
			$tag->setProperty('tag', $item['tag_' . $lang]);
			$tags[$item['id']] = $tag;
		}

		return $tags;
	}

	/**
	 * @var int $photoId
	 * @var bool $withLabel
	 * @return array
	 */
	public function findByPhoto(int $photoId, bool $withLabel = false): array
	{
		$lang = Session::get('lang');
		$sql  = "
			SELECT pt.tag_id, t.tag_" . $lang . " AS tag
			FROM photo_tags pt
			LEFT JOIN tags t ON pt.tag_id = t.id
			WHERE pt.photo_id = :photo_id
			ORDER BY t.tag_" . $lang . "
		";
		$params = [
			'photo_id' => $photoId,
		];
		$this->pdo->query($sql, $params);
		$items = $this->pdo->fetchAll();
		$result = [];

		foreach ($items as $item) {
			if ($withLabel) {
				$result[] = [
					'id'  => $item['tag_id'],
					'tag' => $item['tag'],
				];
			} else {
				$result[] = $item['tag_id'];
			}
		}

		return $result;
	}

	/**
	 * @return array
	 */
	public function findPhotosByTag()
	{
		$sql = "
			SELECT tag_id, COUNT(photo_id) AS nb
			FROM photo_tags
			GROUP BY tag_id
		";
		$this->pdo->query($sql);
		$items = $this->pdo->fetchAll();
		$result = [];

		foreach ($items as $item) {
			$result[$item['tag_id']] = $item['nb'];
		}

		return $result;
	}

	/**
	 * @param int $photoId
	 * @param int $tagId
	 */
	public function insertPhotoTag(int $photoId, int $tagId)
	{
		$sql = "
			INSERT INTO photo_tags (
			    photo_id,
			    tag_id
			) VALUES (
			    :photo_id,
			    :tag_id
			)
		";
		$params = [
			'photo_id' => $photoId,
			'tag_id'   => $tagId,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @param int $photoId
	 * @param int $photoId
	 */
	public function deletePhotoTags(int $photoId)
	{
		$sql = "
			DELETE FROM photo_tags
			WHERE photo_id = :photo_id
		";
		$params = [
			'photo_id'   => $photoId,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @param int $photoId
	 * @param int $tagId
	 */
	public function deleteTagPhotos(int $tagId)
	{
		$sql = "
			DELETE FROM photo_tags
			WHERE tag_id = :tag_id
		";
		$params = [
			'tag_id'   => $tagId,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @return array
	 */
	public function findCountries(string $letter)
	{
		$sql = "
			SELECT *
			FROM countries
			WHERE country_name LIKE '" . $letter . "%'
		";
		$this->pdo->query($sql);

		return $this->pdo->fetchAll();
	}
}
