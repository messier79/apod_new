<?php

namespace Repositories;

use Helpers\Db;
use Models\Model;

/**
 * Class Repository
 * @package Repositories
 */
class Repository
{
	/**
	 * @var Db
	 */
	protected $pdo;

	/**
	 * @var string
	 */
	protected $className;

	/**
	 * Repository constructor.
	 */
	public function __construct()
	{
		$this->pdo = Db::getInstance();
		$this->className = null;
	}

	/**
	 * @param array $orderBy
	 * @return array|null
	 */
	public function fetchAll(array $orderBy = [])
	{
		/**
		 * @var Model $object
		 */
		$result    = [];
		$object    = new $this->className();
		$idField   = $object->getIdField();
		$tableName = preg_replace('/\\\\Models\\\\/', '', $object->getTableName());

		if ($this->className !== null) {
			$sql = "
				SELECT *
				FROM " . $tableName . "
				WHERE deleted_at IS NULL
			";
			$this->addOrderByClause($sql, $orderBy);
			$this->pdo->query($sql);
			$result = $this->pdo->fetchAll();

			foreach ($result as &$item) {
				$item = new $this->className($item[$idField]);
			}
		}

		return $result;
	}

	/**
	 * @param string $sql
	 * @param array $orderBy
	 */
	protected function addOrderByClause(string &$sql, array $orderBy)
	{
		if (!empty($orderBy)) {
			$sql       .= " ORDER BY ";
			$nbClauses = 0;

			foreach ($orderBy as $field => $sort) {
				if ($nbClauses > 0) {
					$sql .= ", ";
				}
				$sql .= $field . " " . $sort;
				$nbClauses++;
			}
		}
	}
}