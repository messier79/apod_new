<?php

namespace Repositories;

use Helpers\Session;
use Helpers\Tools;
use Models\Tag;

class CronExecutionRepository extends Repository
{
	const PER_PAGE = 14;

	public function __construct()
	{
		parent::__construct();
	}

	public function paginate(int $page, int $perPage = null, $searchParams = [])
	{
		if ($perPage === null) {
			$perPage = self::PER_PAGE;
		}

		$params = [];
		$filter = "";

		if (count($searchParams) > 0) {
			$filterParams = $this->buildFilters($searchParams);
			$filter       = $filterParams['filter'];
			$params       = $filterParams['params'];
		}

		if (isset($searchParams['sort_by'])) {
			$sortBy = $searchParams['sort_by'];
		} else {
			$sortBy = "created_at";
		}

		if (isset($searchParams['sort_order'])) {
			$sortOrder = strtoupper($searchParams['sort_order']);
		} else {
			$sortOrder = "DESC";
		}

		$offset     = ($page - 1) * $perPage;
		$operations = [];
		$sql        = "
			SELECT SQL_CALC_FOUND_ROWS *
			FROM cron_executions
			WHERE deleted_at IS NULL
			" . $filter . "
			ORDER BY " . $sortBy . " " . $sortOrder . "
			LIMIT " . $offset . ", " . $perPage . "
		";
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as &$item) {
			$item['decoded_output'] = json_decode($item['output'], true);
		}

		$sql    = "SELECT FOUND_ROWS()";
		$params = [];
		$this->pdo->query($sql, $params);
		$foundRows = $this->pdo->fetch();
		$nbRows    = (int)$foundRows[0];
		$nbPages   = $nbRows / $perPage;

		if (Tools::isDecimal($nbPages)) {
			$nbPages++;
		}

		$nbPages = (int)$nbPages;

		if ($page > 1) {
			$previousPage = $page - 1;
		} else {
			$previousPage = 1;
		}

		if ($page < $nbPages) {
			$nextPage = $page + 1;
		} else {
			$nextPage = $nbPages;
		}

		return [
			'items'        => $result,
			'nbPages'      => $nbPages,
			'nbItems'      => $nbRows,
			'previousPage' => $previousPage,
			'nextPage'     => $nextPage,
		];
	}

	/**
	 * @param string $cron
	 * @param \DateTime $date
	 * @return mixed
	 */
	public function findByDate(string $cron, \DateTime $date)
	{
		$sql    = "
			SELECT *
			FROM cron_executions
			WHERE created_at >= :start_date
			AND created_at <= :end_date
			AND cron = :cron
		";
		$params = [
			'start_date' => $date->format('Y-m-d 00:00:00'),
			'end_date'   => $date->format('Y-m-d 23:59:59'),
			'cron'       => $cron,
		];
		$this->pdo->query($sql, $params);

		return $this->pdo->fetch();
	}

	/**
	 * @return array
	 */
	public function findLatest(int $limit)
	{
		$limitDate = new \DateTime();
		$limitDate->sub(new \DateInterval('P' . $limit . 'D'));
		$sql    = "
			SELECT *
			FROM cron_executions
			WHERE created_at >= :limit_date
			ORDER BY created_at DESC
		";
		$params = [
			'limit_date' => $limitDate->format('Y-m-d 00:00:00'),
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as &$item) {
			$item['decoded_output'] = json_decode($item['output'], true);
		}

		return $result;
	}

	/**
	 * @param string $cron
	 * @param string $output
	 * @return void
	 */
	public function insert(string $cron, string $output)
	{
		$sql    = "
			INSERT INTO cron_executions (
			    cron,
			    output,
			    created_at,
			    updated_at
			) VALUE (
			    :cron,
			    :output,
			    NOW(),
			    NOW()
			)
		";
		$params = [
			'cron'   => $cron,
			'output' => $output,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @param array $searchParams
	 * @return array
	 * @throws \Exception
	 */
	private function buildFilters(array $searchParams): array
	{
		$filter = "";
		$params = [];

		if (isset($searchParams['start_date'])) {
			$filter          .= " AND created_at >= :start_date";
			$params['start_date'] = $searchParams['start_date'] . " 00:00:00";
		}

		if (isset($searchParams['_date'])) {
			$filter          .= " AND created_at >= :end_date";
			$params['end_date'] = $searchParams['end_date'] . " 23:59:59";
		}

		return [
			'filter' => $filter,
			'params' => $params,
		];
	}
}
