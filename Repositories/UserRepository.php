<?php

namespace Repositories;

use Models\User;

class UserRepository extends Repository
{
	const ADMIN_GROUP_ID = 1;
	
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $params
	 * @return User[]
	 */
	public function fetchAll(array $params = []): array
	{
		if (isset($params['sortBy'])) {
			$sortBy = $params['sortBy'];
		} else {
			$sortBy = 'username';
		}

		if (isset($params['sortOrder'])) {
			$sortOrder = $params['sortOrder'];
		} else {
			$sortOrder = 'ASC';
		}

		$users = [];
		$sql         = "
			SELECT u.*, ug.name AS group_name
			FROM users u
			LEFT JOIN user_groups ug ON ug.id = u.group_id
			WHERE u.deleted_at IS NULL
			ORDER BY u." . $sortBy . " " . $sortOrder . "
		";
		$params      = [];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetchAll();

		foreach ($result as $item) {
			$user = new User($item['id']);
			$user->group = $item['group_name'];
			$users[$item['id']] = $user;
		}

		return $users;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @return User|false
	 */
	public function getUserByUsernameAndPassword(string $username, string $password)
	{
		$sql    = "
			SELECT *
			FROM users
            WHERE username = :username
			AND deleted_at IS NULL
		";
		$params = [
			'username' => $username,
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetch();

		if ($result !== false) {
			$isPasswordValid = password_verify($password, $result['password']);

			if (!$isPasswordValid) {
				$result = false;
			} else {
				$user = new User();
				$user->fillPropertiesFromArray($result);
				$result = $user;
			}
		}

		return $result;
	}
	/**
	 * @return bool
	 */
	public function isAdmin(User $user): bool
	{
		$sql    = "
			SELECT ug.id
			FROM users u
			LEFT JOIN user_groups ug on ug.id = u.group_id
            WHERE u.id = :user_id
			AND u.deleted_at IS NULL
		";
		$params = [
			'user_id' => $user->getProperty('id'),
		];
		$this->pdo->query($sql, $params);
		$result        = $this->pdo->fetch();
		$groupId = (int)$result['id'];

		return $groupId === self::ADMIN_GROUP_ID;
	}

	/**
	 * @param string $token
	 * @return AdminUser|null
	 */
	public function findByToken(string $token)
	{
		$user   = null;
		$sql    = "
			SELECT id
			FROM users
			WHERE SHA2(CONCAT(id, username, password, email), 256) = :token
		";
		$params = [
			'token' => $token,
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetch();

		if ($result !== null) {
			$user = new User($result['id']);
		}

		return $user;
	}

	/**
	 * @param User $user
	 * @return string|null
	 */
	public function getToken(User $user)
	{
		$token  = null;
		$sql    = "
			SELECT SHA2(CONCAT(id, username, password, email), 256) AS token
			FROM users
			WHERE id = :id
		";
		$params = [
			'id' => $user->getId(),
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetch();

		if ($result !== null) {
			$token = $result['token'];
		}

		return $token;
	}
}