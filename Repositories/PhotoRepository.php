<?php

namespace Repositories;

use Helpers\Session;
use Helpers\Tools;
use Models\Photo;
use Models\Tag;

class PhotoRepository extends Repository
{
	const PER_PAGE = 50;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param int $year
	 * @param bool $first
	 * @return array
	 * @throws \Exception
	 */
	public function findAllByYear(int $year, bool $first = false): array
	{
		$sql    = "
			SELECT id, date
			FROM photos
			WHERE deleted_at IS NULL
			AND date BETWEEN :date1 AND :date2
			ORDER BY date
		";
		$params = [
			'date1' => $year . '-01-01',
			'date2' => $year . '-12-31',
		];

		if ($first) {
			$sql .= " LIMIT 1";
		}

		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos = [];

		foreach ($results as $result) {
			$photo = new Photo($result['id']);
			$date  = new \DateTime($result['date']);
			$url   = 'day/' . $date->format('ymd');

			$photo->setProperty('url', $url);

			$photos[] = $photo;
		}

		return $photos;
	}

	/**
	 * @param int $year
	 * @param bool $first
	 * @return array
	 * @throws \Exception
	 */
	public function findAllByMonth(int $year, int $month, bool $first = false): array
	{
		$startDate = new \DateTime($year . '-' . $month . '-01');
		$endDate = clone $startDate;
		$endDate->modify('last day of this month');

		$sql    = "
			SELECT id, date
			FROM photos
			WHERE deleted_at IS NULL
			AND date BETWEEN :date1 AND :date2
			ORDER BY date
		";
		$params = [
			'date1' => $startDate->format('Y-m-d'),
			'date2' => $endDate->format('Y-m-d'),
		];

		if ($first) {
			$sql .= " LIMIT 1";
		}

		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos = [];

		foreach ($results as $result) {
			$photo = new Photo($result['id']);
			$date  = new \DateTime($result['date']);
			$url   = 'day/' . $date->format('ymd');

			$photo->setProperty('url', $url);

			$photos[] = $photo;
		}

		return $photos;
	}

	/**
	 * @param int $page
	 * @param int $limit
	 * @return array
	 * @throws \Exception
	 */
	public function findLatests(int $page, int $limit): array
	{
		$offset = $page * $limit - $limit;
		$sql    = "
			SELECT id, date
			FROM photos
			WHERE deleted_at IS NULL
			ORDER BY date DESC
			LIMIT " . $offset . ", " . $limit . "
		";
		$this->pdo->query($sql);
		$results = $this->pdo->fetchAll();
		$photos = [];

		foreach ($results as $result) {
			$photo = new Photo($result['id']);
			$date  = new \DateTime($result['date']);
			$url   = 'day/' . $date->format('ymd');

			$photo->setProperty('url', $url);

			$photos[] = $photo;
		}

		return $photos;
	}

	/**
	 * @param \DateTime $date
	 * @return Photo|null
	 */
	public function findByDate(\DateTime $date)
	{
		$photo  = null;
		$sql    = "
			SELECT id
			FROM photos
			WHERE deleted_at IS NULL
				AND filename LIKE :filename
		";
		$params = [
			'filename' => $date->format('ymd%'),
		];
		$this->pdo->query($sql, $params);
		$result = $this->pdo->fetch();

		if (isset($result['id'])) {
			$photo = new Photo($result['id']);
		}

		return $photo;
	}

	/**
	 * @return Photo|null
	 */
	public function findLatest()
	{
		$date = new \DateTime();
		$photo = null;
		$sql   = "
			SELECT p.*
			FROM photos p
			LEFT JOIN years y ON y.id = p.year_id
			WHERE p.deleted_at IS NULL
			AND y.deleted_at IS NULL
			ORDER BY y.name DESC, p.id DESC
			LIMIT 1
		";
		$this->pdo->query($sql);
		$result = $this->pdo->fetch();

		if (isset($result['id'])) {
			$photo = new Photo($result['id']);
			$url   = 'day/' . $date->format('ymd');

			$photo->setProperty('url', $url);
		}

		return $photo;
	}

	/**
	 * @param int $year
	 * @param int $month
	 * @param bool $inverted
	 * @return Photo[]
	 */
	public function findByMonth(int $year, int $month, bool $inverted = false)
	{
		$photo = null;
		$sql   = "
			SELECT id
			FROM photos
			WHERE deleted_at IS NULL
				AND filename LIKE :filename
			ORDER BY filename
		";

		if ($inverted) {
			$sql .= " DESC";
		}
		$filter = substr($year, 2, 2) . sprintf("%02d", $month) . '%';
		$params = [
			'filename' => $filter,
		];
		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos  = [];

		foreach ($results as $result) {
			$photos[] = new Photo($result['id']);
		}

		return $photos;
	}

	/**
	 * @param int $year
	 * @return Photo[]|null
	 */
	public function findByYear(int $year)
	{
		$photo  = null;
		$sql    = "
			SELECT id
			FROM photos
			WHERE deleted_at IS NULL
				AND filename LIKE :filename
			ORDER BY filename DESC
		";
		$filter = substr($year, 2, 2) . '%';
		$params = [
			'filename' => $filter,
		];
		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos  = [];

		foreach ($results as $result) {
			$photos[] = new Photo($result['id']);
		}

		return $photos;
	}

	/**
	 * @param int $latestId
	 * @param int $limit
	 * @return array
	 * @throws \Exception
	 */
	public function findPreviousTen(int $latestId = 1000000, int $limit = 10): array
	{
		$sql    = "
			SELECT id, date
			FROM photos
			WHERE deleted_at IS NULL
			AND id < :latest_id
			ORDER BY date DESC
			LIMIT " . $limit . "
		";
		$params = [
			'latest_id' => $latestId,
		];

		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos = [];

		foreach ($results as $result) {
			$photo = new Photo($result['id']);
			$date  = new \DateTime($result['date']);
			$url   = 'day/' . $date->format('ymd');

			$photo->setProperty('url', $url);
			$fileParts = preg_split('/\./', $photo->getProperty('filename'));
			$thumbPath = '/photo/' . $fileParts[0] . '/' . $fileParts[1];

			if ($photo->getProperty('video') !== '') {
				$background = '/images/play.png';
				$class = ' video';
			} else {
				$background = $thumbPath;
				$class = '';
			}

			$date = new \DateTime($photo->getProperty('date'));

			if (Session::get('lang') === 'fr') {
				$dateStr = $date->format('j F Y');
			} else {
				$dateStr = $date->format('F, j Y');
			}


			$photos[] = [
				'id' => $photo->getProperty('id'),
				'title' => $photo->getProperty('title_' . Session::get('lang')),
				'description' => base64_encode($photo->getProperty('description_' . Session::get('lang'))),
				'background' => $background,
				'date' => $dateStr,
				'url' => $photo->getProperty('url'),
				'class' => $class,
			];
		}

		return $photos;
	}

	/**
	 * @param array $searchParams
	 * @param int $page
	 * @param int|null $perPage
	 * @return Photo[]|null
	 */
	public function search(array $searchParams, int $page, int $perPage = null)
	{
		$params  = [];
		$filters = "";
		$orderBy = $searchParams['sort_by'] . ' ' . $searchParams['sort_order'];

		if ($perPage === null) {
			$perPage = self::PER_PAGE;
		}

		$offset = ($page - 1) * $perPage;

		if (isset($searchParams['year']) && $searchParams['year'] !== '') {
			$years = preg_split('/,/', $searchParams['year']);
			$yearFilters = "";
			$i = 0;
			
			foreach ($years as $year) {
				$yearShort = substr($year, 2, 2);

				if (isset($searchParams['month']) && $searchParams['month'] !== '') {
					if ((string)$yearShort === '') {
						$yearShort = substr(date('Y'), 2, 2);
					}

					$months = preg_split('/,/', $searchParams['month']);
					
					foreach ($months as $month) {
						if ($yearFilters !== "") {
							$yearFilters .= " OR ";
						}
						
						$yearFilters        .= "filename LIKE :filename" . $i;
						$params['filename' . $i] = $yearShort . sprintf('%02d', $month) . "%";
						$i++;
					}
				} elseif ((string)$yearShort !== '') {
					if ($yearFilters !== "") {
						$yearFilters .= " OR ";
					}
					
					$yearFilters        .= "filename LIKE :filename" . $i;
					$params['filename' . $i] = $yearShort . "%";
					$i++;
				}
			}
			
			$filters .= "AND (" . $yearFilters . ")";
		}

		if (isset($searchParams['tag'])) {
			$tags = preg_split('/,/', $searchParams['tag']);
			$filters .= " AND ";
			$tagFilter = "";
			$i = 1;

			foreach ($tags as $tag) {
				if ($tagFilter !== "") {
					$tagFilter .= " OR ";
				}

				$tagFilter .= "pt.tag_id = :tag_id" . $i;
				$params['tag_id' . $i] = $tag;
				$i++;
			}

			if ($i > 1) {
				$filters .= "(" . $tagFilter . ")";
			}
		}

		if (isset($searchParams['keyword'])) {
			$filters                  .= " AND (title_fr LIKE :title_fr OR title_en LIKE :title_en OR description_fr LIKE :description_fr OR description_en LIKE :description_en)";
			$params['title_fr']       = "%" . $searchParams['keyword'] . "%";
			$params['title_en']       = "%" . $searchParams['keyword'] . "%";
			$params['description_fr'] = "%" . $searchParams['keyword'] . "%";
			$params['description_en'] = "%" . $searchParams['keyword'] . "%";
		}

		$sql = "
			SELECT SQL_CALC_FOUND_ROWS p.*
			FROM photos p
			LEFT JOIN photo_tags pt on p.id = pt.photo_id
			WHERE p.deleted_at IS NULL
				" . $filters . "
			GROUP BY p.id
			ORDER BY " . $orderBy . "
			LIMIT " . $offset . ", " . $perPage . "
		";
		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos  = [];

		$sql = "SELECT FOUND_ROWS()";
		$this->pdo->query($sql);
		$foundRows = $this->pdo->fetch();
		$nbRows    = (int)$foundRows[0];
		$nbPages   = $nbRows / $perPage;

		if (Tools::isDecimal($nbPages)) {
			$nbPages++;
		}
		$nbPages = (int)$nbPages;

		if ($page > 1) {
			$previousPage = $page - 1;
		} else {
			$previousPage = 1;
		}

		if ($page < $nbPages) {
			$nextPage = $page + 1;
		} else {
			$nextPage = $nbPages;
		}

		foreach ($results as $result) {
			$parts    = preg_split('/\./', $result['filename']);
			$date     = \DateTime::createFromFormat('ymd', $parts[0]);
			$photos[] = [
				'date'  => $date,
				'photo' => new Photo($result['id']),
			];
		}

		return [
			'items'        => $photos,
			'nbPages'      => $nbPages,
			'nbItems'      => $nbRows,
			'previousPage' => $previousPage,
			'nextPage'     => $nextPage,
		];
	}

	public function getRandom()
	{
		$photo = null;
		$sql   = "
			SELECT *
			FROM photos
			WHERE deleted_at IS NULL
			ORDER BY RAND()
			LIMIT 1
		";
		$this->pdo->query($sql);
		$result = $this->pdo->fetch();

		if (isset($result['id'])) {
			$photo = new Photo($result['id']);
		}

		return $photo;
	}

	/**
	 * @param int $limit
	 */
	public function populateDates(int $limit)
	{
		$sql = "
			SELECT *
			FROM photos
			WHERE deleted_at IS NULL
			AND date IS NULL
			ORDER BY id
			LIMIT " . $limit . "
		";
		$this->pdo->query($sql);
		$items = $this->pdo->fetchAll();

		foreach ($items as $item) {
			$dateStr = \Services\Photo::getDateFromFilename($item['filename']);
			$sql     = "
				UPDATE photos
				SET date = :date
				WHERE id = :id
			";
			$params  = [
				'date' => $dateStr,
				'id'   => $item['id'],
			];
			$this->pdo->query($sql, $params);
		}

		$sql = "
			SELECT COUNT(id) AS nb
			FROM photos
			WHERE deleted_at IS NULL
			AND date IS NULL
		";
		$this->pdo->query($sql);
		$result    = $this->pdo->fetch();
		$remaining = $result['nb'] - $limit;

		if ($remaining < 0) {
			$remaining = 0;
		}

		echo 'Latest: ' . $item['filename'] . ' (id: ' . $item['id'] . ')<br>';
		echo $remaining . ' remaining.';
	}

	/**
	 * @param Tag $tag
	 * @return array
	 */
	public function findByTagInDescription(Tag $tag): array
	{
		$sql = "
			SELECT id
			FROM photos
			WHERE description_en LIKE :tag_en1
			OR title_en LIKE :tag_en2
		";
		$params = [
			'tag_en1' => "%" . $tag->getProperty('tag_en') . "%",
			'tag_en2' => "%" . $tag->getProperty('tag_en') . "%",
		];
		$this->pdo->query($sql, $params);

		return $this->pdo->fetchAll();
	}

	/**
	 * @param string $string
	 * @return array
	 */
	public function findByStringInDescription(string $string): array
	{
		$sql = "
			SELECT id
			FROM photos
			WHERE description_en LIKE :string1
			OR title_en LIKE :string2
		";
		$params = [
			'string1' => "%" . $string . "%",
			'string2' => "%" . $string . "%",
		];
		$this->pdo->query($sql, $params);

		return $this->pdo->fetchAll();
	}

	/**
	 * @param string $string
	 * @return array
	 */
	public function findByCommentUsername(array $params, int $page = 1, int $perPage = null): array
	{
		if ($perPage === null) {
			$perPage = self::PER_PAGE;
		}

		$offset = ($page - 1) * $perPage;

		$sql = "
			SELECT SQL_CALC_FOUND_ROWS p.*
			FROM photos p
			LEFT JOIN comments c on p.id = c.photo_id
			WHERE c.username = :username
			AND p.deleted_at IS NULL
            AND c.deleted_at IS NULL
			LIMIT " . $offset . ", " . $perPage . "
		";
		$params = [
			'username' => $params['username'],
		];
		$this->pdo->query($sql, $params);
		$results = $this->pdo->fetchAll();
		$photos  = [];

		$sql = "SELECT FOUND_ROWS()";
		$this->pdo->query($sql);
		$foundRows = $this->pdo->fetch();
		$nbRows    = (int)$foundRows[0];
		$nbPages   = $nbRows / $perPage;

		if (Tools::isDecimal($nbPages)) {
			$nbPages++;
		}
		$nbPages = (int)$nbPages;

		if ($page > 1) {
			$previousPage = $page - 1;
		} else {
			$previousPage = 1;
		}

		if ($page < $nbPages) {
			$nextPage = $page + 1;
		} else {
			$nextPage = $nbPages;
		}

		foreach ($results as $result) {
			$parts    = preg_split('/\./', $result['filename']);
			$date     = \DateTime::createFromFormat('ymd', $parts[0]);
			$photos[] = [
				'date'  => $date,
				'photo' => new Photo($result['id']),
			];
		}

		return [
			'items'        => $photos,
			'nbPages'      => $nbPages,
			'nbItems'      => $nbRows,
			'previousPage' => $previousPage,
			'nextPage'     => $nextPage,
		];
	}

	/**
	 * @param int $photoId
	 * @return void
	 */
	public function like(int $photoId)
	{
		$sql = "
			UPDATE photos
			SET likes = likes + 1
			WHERE id = :id
		";
		$params = [
			'id' => $photoId,
		];
		$this->pdo->query($sql, $params);
	}

	/**
	 * @return array
	 */
	public function getNbPhotos()
	{
		$sql = "
			SELECT 'global' AS year, COUNT(id) AS total
			FROM photos
			WHERE deleted_at IS NULL
			UNION
			SELECT y.name, COUNT(p.id) AS total
			FROM photos p
			LEFT JOIN years y on y.id = p.year_id
			WHERE p.deleted_at IS NULL
			GROUP BY y.name
		";
		$this->pdo->query($sql);

		return $this->pdo->fetchAll();
	}
}
