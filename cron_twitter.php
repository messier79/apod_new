<?php
require_once "vendor/autoload.php";

use Controllers\MastodonController;
use Controllers\TumblrController;
use Controllers\TwitterController;
use Helpers\Middleware;
use Repositories\CronExecutionRepository;
use Symfony\Component\HttpFoundation\Request;

$middleware = new Middleware();
$repo       = new CronExecutionRepository();
$already    = $repo->findByDate('social', new DateTime());

if ($already === false) {
	$request            = new Request();
	$twitterController  = new TwitterController();
	$mastodonController = new MastodonController();
	$tumblrController   = new TumblrController();
	$blueskyController  = new \Controllers\BlueskyController();
	$result             = [
		'twitter_en'  => false,
		'twitter_fr'  => false,
		'mastodon_en' => false,
		'mastodon_fr' => false,
		'tumblr_en'   => false,
		'tumblr_fr'   => false,
		'bluesky_en'  => false,
		'bluesky_fr'  => false,
	];

	echo 'en<br>';
	echo 'Twitter<br>';

	try {
		$twitterController->dailyPost($request);
		$result['twitter_en'] = true;
	} catch (Exception $e) {
		// Nothing
	}

	echo 'Mastodon<br>';
	$mastodonController->dailyPost($request);
	$result['mastodon_en'] = true;
	sleep(5);

	echo 'Tumblr<br>';
	$tumblrController->dailyPost($request);
	$result['tumblr_en'] = true;
	sleep(5);

	echo 'BlueSky<br>';
	$blueskyController->dailyPost($request);
	$result['bluesky_en'] = true;
	sleep(5);

	echo '<hr>fr<br>';
	$request->request->set('lang', 'fr');

	echo 'Twitter<br>';

	try {
		$twitterController->dailyPost($request);
		$result['twitter_fr'] = true;
	} catch (Exception $e) {
		// Nothing
	}

	echo 'Mastodon<br>';
	$mastodonController->dailyPost($request);
	$result['mastodon_fr'] = true;

	echo 'Tumblr<br>';
	$tumblrController->dailyPost($request);
	$result['tumblr_fr'] = true;

	echo 'BlueSky<br>';
	$blueskyController->dailyPost($request);
	$result['bluesky_fr'] = true;

	echo 'Done';

	$repo->insert('social', json_encode($result));
}
