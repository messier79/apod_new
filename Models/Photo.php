<?php

namespace Models;

use Config\Config;
use Helpers\Tools;
use Models\Traits\Dates;

class Photo extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'photos';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $year_id;

	/**
	 * @var string
	 * @optional
	 */
	protected $date;

	/**
	 * @var string
	 */
	protected $filename;

	/**
	 * @var string
	 * @optional
	 */
	protected $video;

	/**
	 * @var string
	 * @optional
	 */
	protected $title_fr;

	/**
	 * @var string
	 * @optional
	 */
	protected $title_en;

	/**
	 * @var string
	 * @optional
	 */
	protected $description_fr;

	/**
	 * @var string
	 * @optional
	 */
	protected $description_en;

	/**
	 * @var string
	 * @optional
	 */
	protected $image_url;

	/**
	 * @var string
	 * @optional
	 */
	protected $credits;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var string
	 * @optional
	 */
	protected $likes;

	/**
	 * @var string
	 * @optional
	 * @ignore
	 */
	protected $url;


	/**
	 * Photo constructor.
	 * @param null $id
	 */
	public function __construct($id = null)
	{
		return parent::__construct($id);
	}

	public function __toArray(): array
	{
		$config = new Config();
		$baseUrl = $config->getParameter('base_url');
		$parts = preg_split('/\./', $this->filename);
		$url = $baseUrl . 'photo/' . $parts[0] . '/' . $parts[1];
		$videoUrl = '';

		if (is_numeric($this->video)) {
			$videoUrl = 'https://player.vimeo.com/video/' . $this->video;
		} elseif ($this->video !== '') {
			$videoUrl = 'https://www.youtube.com/embed/' . $this->video;
		}

		return [
			'image_url' => $url,
			'video' => $videoUrl,
			'title_fr' => $this->title_fr,
			'title_en' => $this->title_en,
			'description_fr' => $this->description_fr,
			'description_en' => $this->description_en,
			'credits' => $this->credits,
		];
	}
}
