<?php

namespace Models;

use Models\Traits\Dates;

class Comment extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'comments';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $photo_id;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var string
	 */
	protected $comment;


	/**
	 * Album constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		parent::__construct($id);

		return $this;
	}
}
