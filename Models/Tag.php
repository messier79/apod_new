<?php

namespace Models;

use Helpers\Tools;
use Models\Traits\Dates;

class Tag extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'tags';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $tag_en;

	/**
	 * @var string
	 */
	protected $tag_fr;

	/**
	 * @var string
	 * @ignore
	 * @optional
	 */
	protected $tag;


	/**
	 * Album constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		parent::__construct($id);

		return $this;
	}
}
