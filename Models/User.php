<?php

namespace Models;

use Models\Traits\Dates;

class User extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'users';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $group_id;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var string
	 * @optional
	 */
	protected $password;

	/**
	 * @var string
	 * @format email
	 */
	protected $email;


	/**
	 * User constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		return parent::__construct($id);
	}
}
