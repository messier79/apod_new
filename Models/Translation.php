<?php

namespace Models;

use Helpers\Tools;
use Models\Traits\Dates;

class Translation extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'translations';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $lang;

	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var string
	 */
	protected $value;

	/**
	 * @ignore
	 * @var array
	 */
	protected $photos;


	/**
	 * Album constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		parent::__construct($id);

		return $this;
	}
}
