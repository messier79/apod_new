<?php
namespace Models\Traits;

use DateTime;

trait Dates
{
	/**
	 * @var DateTime
	 * @optional
	 */
	protected $created_at;

	/**
	 * @var DateTime
	 * @optional
	 */
	protected $updated_at;

	/**
	 * @var DateTime
	 * @optional
	 */
	protected $deleted_at;

	
	public function getCreatedAt(): DateTime
	{
		return $this->created_at;
	}
	
	public function setCreatedAt(DateTime $date)
	{
		$this->createdAt = $date;
		
		return $this;
	}
	
	public function getUpdatedAt(): DateTime
	{
		return $this->updated_at;
	}
	
	public function setUpdatedAt(DateTime $date)
	{
		$this->updatedAt = $date;
		
		return $this;
	}
	
	public function getDeletedAt(): DateTime
	{
		return $this->deleted_at;
	}
	
	public function setDeletedAt(DateTime $date)
	{
		$this->deletedAt = $date;
		
		return $this;
	}
}