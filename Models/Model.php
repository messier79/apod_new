<?php

namespace Models;

use Helpers\Db;

class Model
{
	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var string
	 */
	protected $idField = 'id';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * Model constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		$idField  = $this->idField;
		$this->$idField = $id;

		if ($id !== null) {
			$this->load($id);
		}

		return $this;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		$idField = $this->getIdField();
		
		return (int)$this->$idField;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$idField = $this->getIdField();
		$this->$idField = (int)$id;
	}

	/**
	 * @param int $id
	 * @return void
	 */
	public function load(int $id)
	{
		$db        = new Db();
		$tableName = $this->getTableName();
		$sql       = "
            SELECT *
            FROM $tableName
            WHERE " . $this->idField . " = :id
        ";
		$db->query($sql, ['id' => $id]);
		$result = $db->fetch();

		if (is_array($result)) {
			foreach ($result as $key => $value) {
				if (!is_numeric($key)) {
					$this->$key = $value;
				}
			}
		}
	}

	/**
	 * @return int
	 */
	public function getMaxId(): int
	{
		try {
			$db        = new Db();
			$tableName = $this->getTableName();
			$sql       = "
            SELECT MAX(" . $this->idField . ") AS id
            FROM $tableName
        ";
			$db->query($sql);
			$result = $db->fetch();

			return $result['id'];
		} catch (\Exception $e) {
			return 0;
		}
	}

	/**
	 * @return string
	 */
	public function getTableName(): string
	{
		return $this->tableName;
	}

	/**
	 * @return string
	 */
	public function getIdField(): string
	{
		return $this->idField;
	}

	/**
	 * @param string $property
	 * @return mixed
	 */
	public function getProperty(string $property)
	{
		return $this->$property;
	}

	/**
	 * @param string $property
	 * @param $value
	 *
	 * @return $this
	 */
	public function setProperty(string $property, $value)
	{
		$this->$property = $value;

		return $this;
	}

	/**
	 * @param array $properties
	 * @return $this
	 */
	public function fillPropertiesFromArray(array $properties): Model
	{
		foreach ($properties as $property => $value) {
			$this->$property = $value;
		}

		return $this;
	}

	/**
	 * @return $this
	 * @throws \ReflectionException
	 */
	public function save(): Model
	{
		$db                = new Db();
		$tableName         = $this->getTableName();
		$fieldAssignments  = "";
		$idField           = $this->idField;
		$fields            = "";
		$values            = "";
		$params            = [];
		$class             = new \ReflectionClass($this->getClassName()); // get class object
		$properties        = $class->getProperties(); // get class properties
		$ignoredProperties = ['tableName', 'id'];

		foreach ($properties as $property) {
			// Skip ignored properties
			$docComment = $property->getDocComment();
			$isIgnored  = strpos($docComment, '@ignore');
			
			if ($isIgnored !== false) {
				continue;
			}
			
			// Skip inherited properties
			if ($property->getDeclaringClass()->getName() !== $class->getName()) {
				continue;
			}
			$propertyName = $property->getName();

			if (in_array($propertyName, $ignoredProperties)) {
				continue;
			}

			if ($fieldAssignments !== "") {
				$fieldAssignments .= ", ";
				$fields           .= ", ";
				$values           .= ", ";
			}

			$fieldAssignments      .= $propertyName . " = :" . $propertyName;
			$fields                .= $propertyName;
			$values                .= ":" . $propertyName;

			if ($this->$propertyName instanceof \DateTime) {
				$propertyValue = $this->$propertyName->format('Y-m-d H:i:d');
			} else {
				$propertyValue = $this->$propertyName;
			}

			$params[$propertyName] = $propertyValue;
		}

		if ((int)$this->$idField !== 0) {
			$params[$idField] = $this->$idField;
			$sql              = "
                UPDATE $tableName
                SET $fieldAssignments
                WHERE " . $idField . " = :id
            ";
		} else {
			$sql = "
                INSERT INTO $tableName ($fields)
                VALUES ($values)
            ";
		}
		$db->query($sql, $params);

		if ((int)$this->$idField === 0) {
			$this->$idField = $db->getLastInsertId();
		}

		return $this;
	}

	public function getClassName()
	{
		return get_called_class();
	}
}