<?php

namespace Models;

use Helpers\Tools;
use Models\Traits\Dates;

class Year extends Model
{
	use Dates;
	
	/**
	 * @var string
	 * @ignore
	 */
	protected $tableName = 'years';

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $directory;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $cover;

	/**
	 * @var \DateTime
	 */
	protected $date;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @ignore
	 * @var array
	 */
	protected $photos;


	/**
	 * Album constructor.
	 * @param int|null $id
	 */
	public function __construct(int $id = null)
	{
		parent::__construct($id);

		return $this;
	}
}
