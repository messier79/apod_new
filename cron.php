<?php
require_once "vendor/autoload.php";

use Controllers\ToolsController;
use Helpers\Middleware;
use Repositories\CronExecutionRepository;
use Symfony\Component\HttpFoundation\Request;

$middleware = new Middleware();
$repo       = new CronExecutionRepository();
$request    = new Request();
$controller = new ToolsController();
$controller->getFromNasa($request);

$result = [
	'nasa_' => true,
];
$repo->insert('nasa', json_encode($result));
