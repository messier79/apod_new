$(document).ready(function () {
    $('.editor').richText();
    $('.editor-fr').richText();
    $('.editor-en').richText();
    $('.editor-credits').richText({height: 100});
	
	$('[data-toggle="tooltip"]').tooltip();

    $('.header-sortable').on('click', function () {
        let field = $(this).data('field');
        let accountId = $('#account_id').val();
        let page = $('#page').val();
        let sortIcons = $('.sort-icon');
        let sortOrder = 'asc';

        $.each(sortIcons, function () {
            if ($(this).hasClass('sort-icon-' + field)) {
                if ($(this).hasClass('fa-sort-amount-down')) {
                    sortOrder = 'desc';
                } else {
                    sortOrder = 'asc';
                }
            }
        });

        let url = '/search-albums/' + page + '/' + field + '/' + sortOrder;

        $(location).attr('href', url);
    });

    $('#btnAddPhoto').on('click', function () {
        let month = $(this).data('month');
        let year  = $(this).data('year');

        $(location).attr('href', '/admin/photo/add/' + year + '/' + month);
    });

    $('.btn-edit-photo').on('click', function () {
        let id        = $(this).data('id');
        let newWindow = $(this).data('new');
        let url       = '/admin/photo/' + id + '/edit';

        if (newWindow) {
            window.open(url, '_blank');
        } else {
            $(location).attr('href', url);
        }
    });

    $('.btn-home').on('click', function () {
        $(location).attr('href', '/');
    });

    $('.btn-next').on('click', function () {
        let next = $(this).data('next');

        $(location).attr('href', '/day/' + next);
    });

    $('.btn-previous').on('click', function () {
        let previous = $(this).data('previous');

        $(location).attr('href', '/day/' + previous);
    });

    $('.search-year').on('click', function () {
        addItemSelection($(this), 'year');

        if ($('#year').val() !== '' && $('.month-group').hasClass('disabled')) {
            $('.month-group').removeClass('disabled');
        } else if ($('#year').val() === '' && !$('.month-group').hasClass('disabled')) {
            $('.month-group').addClass('disabled');
        }
    });

    $('.search-month').on('click', function () {
        addItemSelection($(this), 'month');
    });

    $('.search-tag').on('click', function () {
        addItemSelection($(this), 'tag');
    });

    $('.btn-tag').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/search-by-tag/' + id);
    });

    $('#tagLabel').on('click', function () {
        $('#tagsContainer').toggleClass('hidden');
        $('#tagDownArrow').toggleClass('hidden');
        $('#tagUpArrow').toggleClass('hidden');
    });

    $('.btn-navigation-admin').on('click', function () {
        let month = $(this).data('month');
        let year = $(this).data('year');

        $(location).attr('href', '/admin/photos/' + year + '/' + month);
    });

    $('.btn-navigation-gallery').on('click', function () {
        let month = $(this).data('month');
        let year = $(this).data('year');

		if (typeof month === 'undefined') {
			$(location).attr('href', '/calendar/' + year);
		} else {
			$(location).attr('href', '/gallery/' + year + '/' + month);
		}
    });

    $('.month').on('click', function () {
        let year  = $(this).data('year');
        let month = $(this).data('month');

        $(location).attr('href', '/gallery/' + year + '/' + month);
    });

    $('.year').on('click', function () {
        let year = $(this).data('year');

        $(location).attr('href', '/calendar/' + year);
    });

    $('.thumb').on('mouseover', function () {
        let dateContainer = $(this).find('.date');

        dateContainer.show();
    });

    $('.thumb').on('mouseout', function () {
        let dateContainer = $(this).find('.date');

        dateContainer.hide();
    });

    $('.thumb').on('click', function () {
        let url = $(this).data('url');

        $(location).attr('href', url);
    });

    $('.photo a').on('click', function (e) {
        e.preventDefault();
        $(this).ekkoLightbox();
    });

    $('.btn-download').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/download/' + id);
    });

    $('.btn-rss').on('click', function () {
        $(location).attr('href', '/rss');
    });

    $('.add-comment').on('click', function () {
        $('#commentForm').removeClass('hidden');
    });

    $('.cancel-comment').on('click', function () {
        $('#commentForm').addClass('hidden');
        $('.comment-error').addClass('hidden');
    });

    $('.btn-save-comment').on('click', function (e) {
        e.preventDefault();

        let now = new Date();
        let month = now.getMonth()+1;
        let day = now.getDate();
        let hour = now.getHours();
        let minute = now.getMinutes();

        if (month < 10) {
            monthStr = ucFirst(now.toLocaleString(lang, { month: 'long' }));
        }

        if (day < 10) {
            day = '0' + day;
        }

        if (hour < 10) {
            hour = '0' + hour;
        }

        if (minute < 10) {
            minute = '0' + minute;
        }

        let dateStr = monthStr  + " "
            + day + ", "
            + now.getFullYear() + " - "
            + hour + ":"
            + minute;

        if (lang === 'fr') {
            dateStr = day + " "
                + monthStr  + " "
                + now.getFullYear() + " - "
                + hour + ":"
                + minute;
        }

        let data = {};
        data.id = 0;
        data.fromSite = true;
        data.username = $('#commentForm').find('form').find('#username').val();
        data.comment = $('#commentForm').find('form').find('#comment').val();
        data.photo_id = $('#commentForm').find('form').find('#photo_id').val();

        if (data.username !== '' && data.comment !== '' ) {
            $('.comment-error').addClass('hidden');

            $.ajax({
                type: "POST",
                url: '/save-comment',
                data: data,
                success: function () {
                }
            });

            let target = $('.comments').find('.list');
            let newElt = target.prepend($('#commentContainer').clone()
                .removeAttr('id')
                .removeClass('hidden')
            );
            newElt.children(":first").find('.username').html(data.username);
            newElt.children(":first").find('.content').html(data.comment);
            newElt.children(":first").find('.creation-date').html(dateStr);
        } else {
            $('.comment-error').removeClass('hidden');
        }

        $('#commentForm').find('form').find('#username').val('');
        $('#commentForm').find('form').find('#comment').val('');
        $('#commentForm').addClass('hidden');
    });

    $('.thumbs-up').on('click', function () {
        if (!$(this).hasClass('text-success')) {
            let photoId = $(this).data('id');
            let data = {};
            data.photo_id = photoId;

            $.ajax({
                type: "POST",
                url: '/like',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.success === 'true') {
                        let likes = parseInt($('.likes').html()) + 1;
                        $('.likes').html(likes);
                    }
                }
            });
            $(this).addClass('text-success');
        }
    });

    $('.show-social-icons').on('click', function () {
        let id = $(this).data('id');
        let container = $("#collapseSocial" + id);

        $(this).prop('style', 'display: none!important');

        if (container.width() === 0) {
            container.animate({width: 382, marginRight: 0}, {duration: 1000});
        } else {
            container.animate({width: 0, marginRight: 0}, {duration: 1000});
        }
    });

    $('.photo-container').find('.btn-share').on('click', function() {
        openShareUrl($(this));
    });
});

function addItemSelection(elt, type)
{
	let selectedValue = elt.data('value');
	let value = $('#' + type).val();

	if (elt.hasClass('selected')) {
		value = value.replace(selectedValue, "")
					 .replace(",,", ",")
					 .replace(/^,|,$/g, "");

		if (value === ',') {
			value = '';
		}

		elt.removeClass('selected');
	} else {
		if (value.length > 0) {
			value += ',';
		}

		value += selectedValue;

		elt.addClass('selected');
	}

	$('#' + type).val(value);
}

function ucFirst(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}

function openShareUrl(elt) {
    let pageUrl = elt.data('url');
    let social = elt.data('social');
    let tweet = encodeURIComponent(elt.data('title'));

    if (social === 'facebook') {
        url = "https://www.facebook.com/sharer.php?p[title]=" + tweet + "p[url]=" + pageUrl;
    } else if (social === 'twitter') {
        url = "https://twitter.com/intent/tweet?url=" + pageUrl + "&text=" + tweet;
    } else if (social === 'linkedin') {
        url = "https://www.linkedin.com/sharing/share-offsite/?url=" + pageUrl;
    } else if (social === 'tumblr') {
        url = 'http://tumblr.com/widgets/share/tool?shareSource=legacy&posttype=link&canonicalUrl=' + pageUrl;
    } else if (social === 'bluesky') {
        url = 'https://bsky.app/intent/compose?text=' + pageUrl;
    }

    window.open(url);
}