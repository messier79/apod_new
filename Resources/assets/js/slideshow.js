$(document).ready(function () {
    $('.btn-next-slide').on('click', function () {
        $('#auto').val(0);
        loadSlide('next');
    });

    $('.btn-previous-slide').on('click', function () {
        $('#auto').val(0);
        loadSlide('previous');
    });

    setTimeout(function(){loadNext();}, 5000);
});

function loadSlide(type)
{
    let current = $('#current').val();
    let year = $('#year').val();
    let month = $('#month').val();
    let params = {
        current: current,
        year: year,
        month: month,
        type: type
    };

    if (typeof $('#random').val() !== 'undefined') {
        params.random = true;
    }

    $.ajax({
        type: "POST",
        url: '/get-slide',
        data: params,
        dataType: 'json',
        success: function (data) {
            if (data.success === 'true') {
                $('#photo').attr('src', '/photo/' + data.filename.replace('.', '/'));
                $('#title').html(data.title);
                $('#link').attr('href', '/day/' + data.date);
                $('#current').val(data.date);
            }
        }
    });
}

function loadNext()
{
    let auto = $('#auto').val();

    if (parseInt(auto) === 1) {
        loadSlide('next');

        setTimeout(function () {
            loadNext();
        }, 5000);
    }
}

function play()
{
    $('#auto').val(1);

    loadNext();
}

function pause()
{
    $('#auto').val(0);
}

function invertColors()
{
    if (!$('.content').hasClass('bg-dark')) {
        $('.content').addClass('bg-dark');
        $('.btn-previous-slide img').attr('src', '/images/angle-left-white.svg');
        $('.btn-next-slide img').attr('src', '/images/angle-right-white.svg');
        $('#title').css('color', '#fff');
    } else {
        $('.content').removeClass('bg-dark');
        $('.btn-previous-slide img').attr('src', '/images/angle-left.svg');
        $('.btn-next-slide img').attr('src', '/images/angle-right.svg');
        $('#title').css('color', '#112');
    }
}
