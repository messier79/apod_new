$(document).ready(function () {
    var done = [];
    var loading = false;
    let limit = 350 * 4;

    $(window).scroll(function () {
        if (!loading && $(window).scrollTop() > $(document).height() - limit) {
            let latestId = $('#latestId').val();
            loading = true;
            $.ajax({
                type: "POST",
                url: "/get-previous-for-scroll",
                data: {latest_id: latestId},
                success: function (response) {
                    let photos   = $.parseJSON(response);
                    let template = null;

                    for (let i = 0 ; i < photos.length ; i++) {
                        addScrollPhoto(photos[i]);
                    }
                    $('#latestId').val(photos[photos.length - 1].id);
                    loading = false;
                }
            });
        }
    });
});

function addScrollPhoto(photo)
{
    let template = $('#articleTemplate').html();
    template = template.replace(/--id--/g, photo.id)
        .replace('--class--', photo.class)
        .replace('--background--', photo.background)
        .replace(/--url--/g, photo.url)
        .replace('--date--', photo.date)
        .replace(/--title--/g, photo.title)
        .replace('--description--', atob(photo.description));

    $('#articles').last().append(template).find('.thumb').one('click', function() {
        let url = $(this).data('url');

        $(location).attr('href', url);
    });
    $('#articles').last().find('#btnCollapse_' + photo.id).on('click', function() {
        addCollapseEvent($(this));
    });
    $('#articles').last().find('.btn-share').on('click', function() {
        openShareUrl($(this));
    });
}

function addCollapseEvent(elt)
{
    let span = elt.find('span');

    if (span.hasClass('fa-arrow-down')) {
        span.removeClass('fa-arrow-down');
        span.addClass('fa-arrow-up');
    } else {
        span.removeClass('fa-arrow-up');
        span.addClass('fa-arrow-down');
    }
}
