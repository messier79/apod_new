(function($) {

    $.fn.extend({
        img_lightbox_tooltip: function(options) {
            options = $.extend( {}, $.MyFunc.defaults, options );

            this.each(function() {
				let src = $(this).data('href');
                new $.MyFunc(this, src, options);
            });
            return this;
        }
    });

    // ctl is the element, options is the set of defaults + user options
    $.MyFunc = function(ctl, src, options) {
			if ( $(ctl).attr("src") ) {
				let imgsrc = $(ctl).attr("src");
				$(ctl).wrap("<div style='pointer:cursor'>");
				
				let that = $(ctl).parent();
				
				$(that).click(function() {
					$(".lightbox-content img").attr("src", src);
					$("#demoLightbox").lightbox();
				})
				//$(that).tooltip({ placement: options.placement, title : options.title})
				//if (options.tooltip_show == "always") { 
				//	$(that).tooltip('show')
				//}
			}
    };

    // option defaults
    $.MyFunc.defaults = {
        //...hash of default settings...
		placement : "top", // top | bottom | right | left
		tooltip_show : "always", // always | hover
		title : "click to enlarge"
    };

})(jQuery);

