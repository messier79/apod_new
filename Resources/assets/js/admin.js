$(document).ready(function () {
    $('#btnAddUser').on('click', function () {
        $(location).attr('href', '/admin/user/add');
    });

    $('.btn-edit-user').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/user/' + id);
    });

    $('.btn-delete-user').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/delete-user/' + id);
    });

    $('#btnAddTag').on('click', function () {
        $(location).attr('href', '/admin/tag/add');
    });

    $('.btn-edit-tag').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/tag/' + id);
    });

    $('.btn-delete-tag').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/delete-tag/' + id);
    });

    $('.btn-populate-tag').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/populate-tag/' + id + '/1');
    });

    $('#btnAddTranslation').on('click', function () {
        $(location).attr('href', '/admin/translation/add');
    });

    $('.btn-edit-translation').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/translation/' + id);
    });

    $('.btn-delete-translation').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/delete-translation/' + id);
    });

    $('#btnAddYear').on('click', function () {
        $(location).attr('href', '/admin/year/add');
    });

    $('.btn-edit-year').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/year/' + id);
    });

    $('#yearForm #name').on('change', function () {
        let year = $(this).val();

        $('#yearForm #directory').val(year);
        $('#yearForm #date').val(year + '-01-01');
        $('#yearForm #cover').val(year.toString().substring(2, 4) + '0101.jpg');
    });

    $('.btn-view-comments').on('click', function () {
        let photoId = $(this).data('id');

        $(location).attr('href', '/admin/comments/' + photoId);
    });

    $('.btn-edit-comment').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/comment/' + id);
    });

    $('.btn-delete-comment').on('click', function () {
        let id = $(this).data('id');

        $(location).attr('href', '/admin/delete-comment/' + id);
    });

    $('.btn-navigation-thumbnails').on('click', function () {
        let year = $(this).data('year');

        $(location).attr('href', '/admin/thumbnails/' + year);
    });

    $('.btn-generate-thumbnail').on('click', function () {
        let year = $(this).data('year');
        let month = $(this).data('month');
        let nbLines = $('#nbLines_' + month).val();

        $(location).attr('href', '/admin/generate-thumbnail/' + year + '/' + month + '/' + nbLines);
    });

    $('.btn-delete-backup').on('click', function () {
        let date = $(this).data('date');
        let href = $(this).data('href') + '/' + date;

        $(location).attr('href', href);
    });

    $('.btn-create-backup').on('click', function () {
        $(location).attr('href', '/admin/backup/create');
    });

    $('.btn-translate').on('click', function () {
        var fieldId = $(this).data('type');
        let text = $(this).data('text');
        let data = {
            text: text
        };

        $.ajax({
            type: "POST",
            url: '/translate',
            data: data,
            success: function (response) {
                $('#' + fieldId).removeClass('hidden').val(response);

            }
        });
    });

    // Bind to modal opening to set necessary data properties to be used to make request
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.btn-ok', this).data('id', data.id);
        $('.btn-ok', this).data('href', data.href);
    });

    // Bind click to OK button within popup
    $('#confirm-delete').on('click', '.btn-ok', function (e) {
        let href      = $('.btn-ok').data('href');
        let id        = $('.btn-ok').data('id');
        let url       = href + String(id);

        $('.btn-ok').addClass('disabled');
        $('.btn-ok').html($('#ajaxLoader').html());

        document.location = url;
    });
});
