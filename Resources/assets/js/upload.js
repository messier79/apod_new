$(document).ready(function()
{
    $("#drop-area").on('dragenter', function (e){
        e.preventDefault();
        $(this).css('background', '#BBD5B8');
    });

    $("#drop-area").on('dragover', function (e){
        e.preventDefault();
    });

    $("#drop-area").on('drop', function (e){
        $(this).css('background', '#D8F9D3');
        e.preventDefault();
        let image = e.originalEvent.dataTransfer.files;

        createFormData(image);
    });

    $("#photo").on('change', function (e){
        e.preventDefault();

        createFormData($(this)[0].files);
    });
});

function createFormData(image)
{
    let formImage = new FormData();
    formImage.append('photo', image[0]);
    formImage.append('filename', $('#filename').val());

    uploadFormData(formImage);
}

function uploadFormData(formData)
{
    $.ajax({
        url: "/tools/upload-image",
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(data){
            let json = $.parseJSON(data);

            if (json.success === 'true') {
                let src = '/photo/' + json.date_str + '/' + json.extension + '/thumb';

                $('#preview').attr('src', src);
                $('#drop-area').html('File successfully uploaded');
            } else {
                $('#drop-area').html('An error happened during the upload');
            }
        }});
}